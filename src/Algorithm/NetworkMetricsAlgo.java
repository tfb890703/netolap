package Algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import network.Node;
import network.NodeType;
import network.Subnetwork;

public class NetworkMetricsAlgo extends BasicAlgorithm{

	public NetworkMetricsAlgo(Subnetwork subnetwork) {
		super(subnetwork);
		this.setResultFormatType(ResultFormatType.independentStructure);
		
		this.result = new NetworkMetricsAlgoResult();
	}

	@Override
	public void calculation() {
		//Node Count
		int nodeCount = this.subnetwork.getAllNodes().size();
		((NetworkMetricsAlgoResult)result).addMetric("Node Count", nodeCount);
				
		//Node Count for each type
		HashMap<String, Integer> nodeCountForTypes = new HashMap<String, Integer>();
		for (NodeType nt : this.subnetwork.getNodes().keySet())
		{
			nodeCountForTypes.put(nt.getName(), this.subnetwork.getNodesByType(nt).getNodeCount());
		}
		((NetworkMetricsAlgoResult)result).addMetric("Typed Node Count", nodeCountForTypes);
		
		//Edge Count
		//Average degree for each type
		Map<Node, Map<NodeType, ArrayList<Node>>> edges = this.subnetwork.getLinks();
		Map<String, Integer> typedEdges = new HashMap<String, Integer>(); 
		int edgeCount = 0;
		for (Node n : edges.keySet()){
			for (NodeType nt : edges.get(n).keySet()){
				edgeCount += edges.get(n).get(nt).size();
				if (typedEdges.containsKey((n.getType().getName()))){
					typedEdges.put(n.getType().getName(), typedEdges.get(n.getType().getName()) + edges.get(n).get(nt).size());
				}
				else {
					typedEdges.put(n.getType().getName(), edges.get(n).get(nt).size());
				}
			}
		}
		Map<String, Double> typedAvgEdges = new HashMap<String, Double>();
		for (String str : typedEdges.keySet()){
			typedAvgEdges.put(str, ((double)typedEdges.get(str)) / nodeCountForTypes.get(str));
		}
		((NetworkMetricsAlgoResult)result).addMetric("Edge Count", edgeCount / 2);
		((NetworkMetricsAlgoResult)result).addMetric("Average Edge Count", ((double)edgeCount) / nodeCount);
		((NetworkMetricsAlgoResult)result).addMetric("Average Typed Edge Count", typedAvgEdges);
		
		
		//TODO: more algorithms (What if the graph is not connected?)
		//Connectivity
		
		//Diameter
		
		//Centrality
	}

}


