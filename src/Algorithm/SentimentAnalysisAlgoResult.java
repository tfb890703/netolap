package Algorithm;

import java.util.HashMap;

import network.Node;
import network.NodeType;

public class SentimentAnalysisAlgoResult {
	public HashMap<Node, Double> sentiScores = new HashMap<Node, Double>();
	public HashMap<NodeType, HashMap<Node, Double>> sentiScoreByType = new HashMap<NodeType, HashMap<Node,Double>>();
}
