package Algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import algorithm_metric.SavedMetric;

import network.Node;
import network.NodeType;
import network.Subnetwork;

/**
 * A basic algorithm class to store basic information
 * @author Fangbo Tao
 * 
 */
public abstract class BasicAlgorithm {
	
	Subnetwork subnetwork;	// The target sub-network
	ResultFormatType resultFormatType;

    //For different types
	Object result;
	
	public BasicAlgorithm(Subnetwork subnetwork) {
		this.subnetwork = subnetwork;
	}
	
	
	public Object getResult() {
		return result;
	}
	
	/**
	 * Set 
	 * @param resultFormatType The format of the result
	 * @param scoreType	If the result is score, specify the score type, might be integer or real
	 */
	public void setResultFormatType(ResultFormatType resultFormatType, ScoreType scoreType) {
		this.resultFormatType = resultFormatType;
		
		// Initialize result
		switch (resultFormatType) {
		case networkScore:
			if (scoreType == ScoreType.integer)
				this.result = 0;
			else if (scoreType == ScoreType.real)
				this.result = 0.0;
			break;
		case typeScore:
			if (scoreType == ScoreType.integer)
				this.result = new HashMap<NodeType, Integer>();
			else if (scoreType == ScoreType.real)
				this.result = new HashMap<NodeType, Double>();
			break;
		case nodeScore:
			if (scoreType == ScoreType.integer)
				this.result = new HashMap<Node, Integer>();
			else if (scoreType == ScoreType.real)
				this.result = new HashMap<Node, Double>();
			break;
		case nodeStructure:
			break;
		case independentStructure:
			break;
		default:
			break;
		}
	}
	
	public void setResultFormatType(ResultFormatType resultFormatType){
		this.setResultFormatType(resultFormatType, ScoreType.real);
	}
	
	
	abstract public void calculation();
	
}

enum ScoreType{
	integer,
	real
}


