package Algorithm;

import java.util.HashMap;

public class NetworkMetricsAlgoResult {
	
	public HashMap<String, Object> metrics;
	
	public NetworkMetricsAlgoResult(){
		metrics = new HashMap<String, Object>();
	}
	
	public void addMetric(String metricName, Object value){
		metrics.put(metricName, value);
	}
}
