package Algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import network.Node;
import network.Subnetwork;

public class RankingAlgo extends BasicAlgorithm {
	
	//PageRank parameters
	double dampingFactor = 0.85;
	int maxIteration = 100;
	double minDelta = 0.00001;

	
	
	public RankingAlgo(Subnetwork subnetwork) {
		super(subnetwork);
		this.setResultFormatType(ResultFormatType.nodeScore);
	}
	
	
	@SuppressWarnings("unchecked")
	public void calculation() {
		ArrayList<Node> allNodes = this.subnetwork.getAllNodes();
		Map<Node, List<Node>> neighborMap = new HashMap<Node, List<Node>>();
		int size = allNodes.size();
		double min_value = (1.0 - dampingFactor) / size;
		
		HashMap<Node, Double> tempResult = new HashMap<Node, Double>();
		result = new RankingAlgoResult();
		
		for (Node node : allNodes){
			tempResult.put(node, 1.0 / size);
			neighborMap.put(node, subnetwork.getAllLinksByNode(node));
		}
		
		// Run the iterations
		double diff = 0;
		for (int i = 0; i < maxIteration; i++){
			System.out.println(i + " " + diff);
			diff = 0;
			for (Node node : allNodes){
				double rank = min_value;
				for (Node neighbor : neighborMap.get(node)){
					int neighborSize = neighborMap.get(neighbor).size();
					rank += dampingFactor * tempResult.get(neighbor) / neighborSize;
				}
				diff += Math.abs(tempResult.get(node) - rank);
				tempResult.put(node, rank);
			}
			
			System.out.print("Iteration " + String.valueOf(i) + " is done.");
			if (diff < minDelta){
				break;
			}
		}
		
		((RankingAlgoResult)result).rankingForNode = tempResult;
		for (Node node : tempResult.keySet()){
			if (!((RankingAlgoResult)result).rankingByType.containsKey(node.getType())){
				((RankingAlgoResult)result).rankingByType.put(node.getType(), new HashMap<Node, Double>());
				((RankingAlgoResult)result).rankingByType.get(node.getType()).put(node, tempResult.get(node));
			}
			else{
				((RankingAlgoResult)result).rankingByType.get(node.getType()).put(node, tempResult.get(node));
			}
		}
		
		
	}
	
	public static void main(String[] args) throws Exception{
		List<Class<?>> classList = new ArrayList<Class<?>>();
		classList.add(RankingAlgo.class);
		classList.add(HistogramAlgo.class);
		
		for (Class<?> myClass : classList){
			System.out.print(myClass.getDeclaredField("test").get(myClass));
		}
		
	}
}
