package Algorithm;

import java.util.HashMap;

import network.Node;

public class SimilaritySearchAlgoResult {
	//TODO design the result type
	public HashMap<Node, HashMap<Node, Integer>> linkCount;
	public HashMap<Node, HashMap<Node, Double>> similarityScores;
	
	public SimilaritySearchAlgoResult(){
		linkCount = new HashMap<Node, HashMap<Node, Integer>>();
		similarityScores = new HashMap<Node, HashMap<Node,Double>>();
	}
}
