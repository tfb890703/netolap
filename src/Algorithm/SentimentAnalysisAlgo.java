package Algorithm;

import algorithm_metric.SavedMetric;
import algorithm_metric.SentimentAnalysisMetric;
import network.Node;
import network.NodeSet;
import network.NodeType;
import network.Subnetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SentimentAnalysisAlgo extends BasicAlgorithm{

	public static SentimentAnalysisMetric sentimentAnalysisMetric = new SentimentAnalysisMetric();
    public static Set<SavedMetric> requiredMetrics = new HashSet<SavedMetric>(Arrays.asList(
            sentimentAnalysisMetric));

	public SentimentAnalysisAlgo(Subnetwork subnetwork) {
		super(subnetwork);
		this.setResultFormatType(ResultFormatType.nodeScore);
		
		this.result = new SentimentAnalysisAlgoResult();
	}

	@Override
	public void calculation() {
		NodeType centralType = this.subnetwork.getDs().getCentralNodeType();
		NodeSet centralNodes = this.subnetwork.getNodesByType(centralType);
		Map<Node, Double> sentimentScores = (Map<Node, Double>)(this.subnetwork.getDs().getSavedMetricObject(sentimentAnalysisMetric));
		
		HashMap<Node, Double> scores = new HashMap<Node, Double>();
		HashMap<NodeType, HashMap<Node, Double>> scoresByType = new HashMap<NodeType, HashMap<Node,Double>>();
		ArrayList<Node> nodes = this.subnetwork.getAllNodes();
		for (Node n : nodes){
			if (n.getType() != centralType){
				double totalScore = 0;
				ArrayList<Node> centralNeighbors = this.subnetwork.getLinksByNodeAndType(n, centralType);
				if (centralNeighbors.size() != 0){
					for (Node neighbor : centralNeighbors){
						totalScore += sentimentScores.get(neighbor);
					}
					totalScore /= centralNeighbors.size();
				}
				scores.put(n, totalScore);
				
				if (!scoresByType.containsKey(n.getType())){
					scoresByType.put(n.getType(), new HashMap<Node, Double>());
				}
				scoresByType.get(n.getType()).put(n, totalScore);
			}
		}
		
		//Put it into types
		((SentimentAnalysisAlgoResult)result).sentiScores = scores;
		((SentimentAnalysisAlgoResult)result).sentiScoreByType = scoresByType;
	}

}
