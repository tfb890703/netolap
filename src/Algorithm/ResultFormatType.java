package Algorithm;

/**
 * Enum to define the netOLAP aggregation output
 * @author taofangbo
 */
public enum ResultFormatType {
	networkScore,			// A score for the whole network: total node count, connectivity 
	typeScore,				// A score for each node type: node count, avg_degree
	nodeScore,				// A score for each node: Rank, sentiment analysis
	nodeStructure,			// A data structure for each node: similarity search 
	independentStructure	// An independent data structure: Histogram or word cloud
}
