package Algorithm;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by george on 4/21/14.
 * Copyright George Brova
 */
public class AllAlgorithms {
    Set<Class <? extends BasicAlgorithm>> allAlgorithms = new HashSet<Class <? extends BasicAlgorithm>>();

    public AllAlgorithms() {
        //Every time someone writes an algo, it's their job to add it here.
        allAlgorithms.add(HistogramAlgo.class);
        allAlgorithms.add(RankingAlgo.class);
        allAlgorithms.add(SentimentAnalysisAlgo.class);
        allAlgorithms.add(SimilaritySearchAlgo.class);
    }

    public Set<Class <? extends BasicAlgorithm>> getAlgorithms() {
        return allAlgorithms;
    }
}
