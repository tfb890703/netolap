package Algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import network.Node;
import network.NodeSet;
import network.NodeType;
import network.Subnetwork;

public class SimilaritySearchAlgo extends BasicAlgorithm{
	
	// Sample query:
	// AGGREGATE dm BY similarity WHERE node = "Person.Jiawei Han" AND metapath = "Person-Article-Person"
	
	MetaPath metaPath;
	NodeType targetType;
	Node targetNode;
	
	/**
	 * The map store the parameters, here it includes the meta-paths, node
	 * @param subnetwork
	 * @param args
	 */
	public SimilaritySearchAlgo(Subnetwork subnetwork, Map<String, String> args) throws Exception {
		super(subnetwork);
		this.setResultFormatType(ResultFormatType.nodeStructure);
		
		this.result = new SimilaritySearchAlgoResult();
		
		//Get the target node and the metapath.
		try{
			//Read the parameters
			String nodeInfo = args.get("node");
			String targetTypeString = nodeInfo.split("\\.")[0];
			String nodeName = nodeInfo.substring(nodeInfo.indexOf(".") + 1);
			String[] metaPathElements = args.get("metapath").split("-");
			
			targetType = this.subnetwork.getDs().getNodeTypeByName(targetTypeString);
			targetNode = this.subnetwork.getNodeByTypeAndName(targetType, nodeName);
			
			ArrayList<NodeType> metaPathArray = new ArrayList<NodeType>();
			for (String ntString : metaPathElements)
				metaPathArray.add(this.subnetwork.getDs().getNodeTypeByName(ntString));
			
			metaPath = new MetaPath(metaPathArray); 
			
		}catch (Exception e){
			throw new Exception("Similarity Search Syntax error.");
		}
	}

	@Override
	public void calculation() {
		// Calculate the result based on the target node and its metapath
		NodeSet nodeSet = this.subnetwork.getNodesByType(targetType);
		
		//Compute the self-links
		for (Node node : nodeSet.getNodes()){
			int counter = 0;
			((SimilaritySearchAlgoResult)result).linkCount.put(node, new HashMap<Node, Integer>());
			HashMap<Node, Integer> intermediateResult = new HashMap<Node, Integer>();
			intermediateResult.put(node, 1);
			for (int i = 1; i < metaPath.path.size(); i++){
				NodeType nt = metaPath.path.get(i);
				HashMap<Node, Integer> nextLayerMap = new HashMap<Node, Integer>();
				for (Node currentNode : intermediateResult.keySet()){
					ArrayList<Node> neighbors = this.subnetwork.getLinksByNodeAndType(currentNode, nt);
					for (Node neighbor : neighbors){
						if (nextLayerMap.containsKey(neighbor))
							nextLayerMap.put(neighbor, nextLayerMap.get(neighbor) + intermediateResult.get(currentNode));
						else
							nextLayerMap.put(neighbor, intermediateResult.get(currentNode));
					}
				}
				intermediateResult = nextLayerMap;
			}
			((SimilaritySearchAlgoResult)result).linkCount.put(node, intermediateResult);
		}
		
		//Compute scores
		HashMap<Node, Double> targetScores = new HashMap<Node, Double>();
		for (Node toNode : ((SimilaritySearchAlgoResult)result).linkCount.get(targetNode).keySet()){
			double score = 2.0 * getScoreInt(((SimilaritySearchAlgoResult)result).linkCount, targetNode, toNode) /
					(getScoreInt(((SimilaritySearchAlgoResult)result).linkCount, targetNode, targetNode) + getScoreInt(((SimilaritySearchAlgoResult)result).linkCount, toNode, toNode));
			targetScores.put(toNode, score);
		}
		
		((SimilaritySearchAlgoResult)result).similarityScores.put(targetNode, targetScores);
	}
	
	public Integer getScoreInt(HashMap<Node, HashMap<Node, Integer>> map, Node from, Node to){
		if (map.containsKey(from)){
			if (map.get(from).containsKey(to)){
				return map.get(from).get(to);
			}
		}
		return 0;
	}
	
	public Double getScoreDouble(HashMap<Node, HashMap<Node, Double>> map, Node from, Node to){
		if (map.containsKey(from)){
			if (map.get(from).containsKey(to)){
				return map.get(from).get(to);
			}
		}
		return 0.0;
	}

}


class MetaPath{
	public ArrayList<NodeType> path;
	public MetaPath(ArrayList<NodeType> path) {
		this.path = path;
	}
}


