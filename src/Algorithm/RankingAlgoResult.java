package Algorithm;

import java.util.HashMap;

import network.Node;
import network.NodeType;

public class RankingAlgoResult {
	
	public HashMap<Node, Double> rankingForNode = new HashMap<Node, Double>();
	public HashMap<NodeType, HashMap<Node, Double>> rankingByType = new HashMap<NodeType, HashMap<Node,Double>>();
	
}
