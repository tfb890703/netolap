package algorithm_metric;

import network.DataSet;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by george on 4/21/14.
 * Copyright George Brova
 */
public abstract class SavedMetric {

    public abstract void saveToFile(BufferedWriter writer, DataSet dataSet) throws IOException;
    public abstract Object loadFromFile(BufferedReader reader, DataSet dataSet) throws IOException;

    public String getMetricName() {
        return this.getClass().getName();
    }
}
