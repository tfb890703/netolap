package algorithm_metric;

import network.DataSet;
import network.Node;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by george on 4/21/14.
 * Copyright George Brova
 */
public class SentimentAnalysisMetric extends SavedMetric {

    public double computeSentiment(String text) {
        if (text == null || text.length() == 0) {
            return 0;
        }

        String py_prefix = "python_src/sentiments/";

        String[] parts = new String[3];
        parts[0] = py_prefix + "env/bin/python";
        parts[1] = py_prefix + "main_sentiment.py";
        parts[2] = text;

        double score = 0;
        try {
            Process p = Runtime.getRuntime().exec(parts);

            //Just read one line - that should be the response.
            BufferedReader in = new BufferedReader( new InputStreamReader(p.getInputStream()) );
            String line = in.readLine();
            in.close();
            score = Double.parseDouble(line);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Warning: could not get sentiment score");
        }
        return score;
    }

    @Override
    public void saveToFile(BufferedWriter writer, DataSet dataSet) throws IOException {
        System.out.println("Saving metrics for sentiment analysis. This can take a while");
        for (Node node : dataSet.getRawNetwork().getAllNodes()) {
            String nodeText = dataSet.getAssociatedText(node);
            if (nodeText == null) {
                continue;
            }
            double sentimentScore = computeSentiment(nodeText);
            writer.append(node.getId() + "\t" + sentimentScore + "\n");
        }

    }

    @Override
    public Object loadFromFile(BufferedReader reader, DataSet dataSet) throws IOException {
        //Assuming the line is of the form <nodeid>\t<score>
        Map<Node, Double> sentimentScores = new HashMap<Node, Double>();

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");
            int nodeid = Integer.parseInt(parts[0]);
            double sentimentScore = Double.parseDouble(parts[1]);
            sentimentScores.put(dataSet.getRawNetwork().getNodeById(nodeid), sentimentScore);
        }
        return sentimentScores;
    }

}
