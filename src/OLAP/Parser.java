package OLAP;

import java.awt.print.Printable;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.crypto.Data;

import com.sun.org.apache.xml.internal.security.algorithms.Algorithm;

import Algorithm.BasicAlgorithm;
import Algorithm.NetworkMetricsAlgo;
import Algorithm.RankingAlgo;
import Algorithm.SentimentAnalysisAlgo;
import Algorithm.SimilaritySearchAlgo;
import Algorithm.SimilaritySearchAlgoResult;

import sun.applet.Main;
import network.Constraint;
import network.ConstraintSet;
import network.ConstraintType;
import network.DataSet;
import network.Hierarchy;
import network.Network;
import network.Node;
import network.NodeSet;
import network.NodeType;
import network.Subnetwork;
import network.Constants;
import network.Utils;


//Sample query
//	CREATE NETWORK dm WHERE Author.Area = "DM" AND Conference.Org = "ACM"
//  AGGREGATE dm BY connectivity (WHERE field_A = xx AND field_B = XX and field_C = xx) 

//TODO: We need to write a complete compiler.

public class Parser {
	
	public static int count = 0;
	
	
	/**
	 * Parse the "CREATE NETWORK" clause and generate a sub network
	 * @param ds Data set
	 * @param statement Create Network statement
	 * @return A subnetwork
	 */
	public static Subnetwork parseCreateNetworkClause(DataSet ds, String statement) throws Exception{
		
		//Clean the statement
		statement = cleanStatement(statement);
		
		if (!statement.toUpperCase().startsWith("CREATE NETWORK ")){
			throw new Exception("Statement does not start with CREATE NETWORK");
		}
		
		String[] elementStrings = statement.split(" ");
		if (elementStrings.length < 5){
			throw new Exception("No contraints specified.");
		}
		
		if (!elementStrings[3].toUpperCase().equals("WHERE")){
			throw new Exception("WHERE clause error.");
		}
		
		String netName;
		ArrayList<Constraint> constraints = new ArrayList<Constraint>();
		
		netName = elementStrings[2];
		
		//Extract all the constraints
		Boolean isAND = false;
		for (int i = 4; i < elementStrings.length; i++){
			if (!isAND){
				if (!elementStrings[i].contains("=")){
					throw new Exception("Constraints Error.");
				}
				
				String[] constraintParts = elementStrings[i].split("=");
				constraintParts[1] = constraintParts[1].replace(Constants.SPACE_SUBSTITUTION, " ");
				String[] nodeHierParts = constraintParts[0].split("\\.");
				
				if (constraintParts.length != 2 || nodeHierParts.length != 2) 
					throw new Exception("Constraints Error");
				
				Constraint constraint = new Constraint();
				constraint.setConstraintType(ConstraintType.hierarchy);
				
				NodeType nt = ds.getNodeTypeByName(nodeHierParts[0]);
				Hierarchy hier = nt.getHierarchyByName(nodeHierParts[1]);
				constraint.setNodeType(nt);
				constraint.addHierarchyNode(hier.getHierarchyNodeByName(constraintParts[1]));
				constraints.add(constraint);
				
				isAND = true;
			}
			else{
				if (!elementStrings[i].toUpperCase().equals("AND")){
					throw new Exception("Constraints Error.");
				}
				isAND = false;
			}
		}
		
		ConstraintSet constraintSet = new ConstraintSet(ds);
		constraintSet.addContraints(constraints);
		constraintSet.simplifyConstraints();
		
		// Look up in the dataset first, to see if the subnetwork is built already.
		Subnetwork tempSubNet = ds.getSubnetworkByConstraints(constraintSet);
		if (tempSubNet != null){
			tempSubNet.setName(netName);
			ds.saveSubnetworkByName(tempSubNet, netName);
			count += 1;
			Utils.createJsonForSubnetwork(tempSubNet, count);
			return tempSubNet;
		}
		else{
			//Generate node set
			ArrayList<NodeSet> constraintNodeSets = new ArrayList<NodeSet>();
			for (Constraint c : constraints){
				NodeSet ns = ds.getRawNetwork().getNodes(c);
				constraintNodeSets.add(ns);
			}
			
			
			//Generate sub-network
			Subnetwork sNetwork = ds.getRawNetwork().buildSubnetwork(constraintNodeSets);
			sNetwork.setName(netName);
			
			
			
			ds.saveSubnetwork(sNetwork, constraintSet);
			
			//Temp for json:
			count += 1;
			Utils.createJsonForSubnetwork(sNetwork, count);
			
			return sNetwork;
		}
		
		
	}
	
	
	/**
	 * Parse the "AGGREGATE" clause and calculate the results
	 * Example: AGGREGATE dm BY connectivity (WHERE field_A = xx AND field_B = XX and field_C = xx)
	 * @param ds Data set
	 * @param statement Create Network statement
	 * @return A subnetwork
	 */
	public static Object parseAggregationClause(DataSet ds, String statement) throws Exception{
		//Clean the statement
		statement = cleanStatement(statement);
		
		if (!statement.toUpperCase().startsWith("AGGREGATE ")){
			throw new Exception("Statement does not start with AGGREGATE");
		}
		
		String[] elementStrings = statement.split(" ");
		
		if (elementStrings.length < 4){
			throw new Exception("Syntax error, too short.");
		}
		
		if (!elementStrings[2].equalsIgnoreCase("BY") || (elementStrings.length > 4 && !elementStrings[4].equalsIgnoreCase("WHERE"))){
			throw new Exception("Syntax error.");
		}
		
		String netName = elementStrings[1];
		String algo = elementStrings[3];
		
		Subnetwork subnetwork = ds.getSubnetworkByName(netName);
		
		if (subnetwork == null){
			throw new Exception("Subnetwork " + netName + " doesn't exist.");
		}
		
		HashMap<String, String> args = new HashMap<String, String>();
		if (elementStrings.length > 4){
			if (!elementStrings[4].toUpperCase().equals("WHERE")){
				throw new Exception("WHERE clause error.");
			}
			
			boolean isAND = false;
			for (int i = 5; i < elementStrings.length; i++){
				if (!isAND){
					if (!elementStrings[i].contains("=")){
						throw new Exception("Constraints Error.");
					}
					
					String[] constraintParts = elementStrings[i].split("=");
					constraintParts[1] = constraintParts[1].replace(Constants.SPACE_SUBSTITUTION, " ");
					
					args.put(constraintParts[0], constraintParts[1]);
					
					isAND = true;
				}
				else{
					if (!elementStrings[i].toUpperCase().equals("AND")){
						throw new Exception("Constraints Error.");
					}
					isAND = false;
				}
			}
		}
		
		//TODO find a way to systematically store/show the result
		BasicAlgorithm algorithm = null;
		
		if (algo.equals("similarity")){
			SimilaritySearchAlgo similaritySearchAlgo = new SimilaritySearchAlgo(subnetwork, args);
			similaritySearchAlgo.calculation();
			algorithm = similaritySearchAlgo;
		}
		else if (algo.equals("sentiment")){
			SentimentAnalysisAlgo sentimentAnalysisAlgo = new SentimentAnalysisAlgo(subnetwork);
			sentimentAnalysisAlgo.calculation();
			algorithm = sentimentAnalysisAlgo;
		}
		else if (algo.equals("ranking")){
			RankingAlgo rankingAlgo = new RankingAlgo(subnetwork);
			rankingAlgo.calculation();
			algorithm = rankingAlgo;
		}
		else if (algo.equals("metrics")){
			NetworkMetricsAlgo networkMetricsAlgorithm = new NetworkMetricsAlgo(subnetwork);
			networkMetricsAlgorithm.calculation();
			algorithm = networkMetricsAlgorithm;
		}
		
		return algorithm.getResult();
	}
	
	
	
	public static String cleanStatement(String statement) throws Exception{
		// For the things in "", replace space with special character
		String[] segments = (statement + " ").split("\"");

		for (int i = 0; i < ((segments.length -1) / 2); i++){
			segments[2 * i + 1] = segments[2 * i + 1].replace(" ", Constants.SPACE_SUBSTITUTION);
		}
		
		StringBuilder sBuilder = new StringBuilder();
		for (String s : segments){
			sBuilder.append(s);
		}
		statement = sBuilder.toString();
		statement = statement.replaceAll("\\s+", " ");
		statement = statement.replaceAll("\\s+=\\s+", "=");
		
		return statement;
	}
	
	/**
	 * A super method to determine the parser of the statement
	 * @param ds
	 * @param statement
	 */
	public static Object parseStatement(DataSet ds, String statement){
		try {
			String newStatement = cleanStatement(statement);
			if (newStatement.toLowerCase().startsWith("create network ")){
				return parseCreateNetworkClause(ds, statement);
			}
			else if (newStatement.toLowerCase().startsWith("aggregate ")){
				return parseAggregationClause(ds, statement);
			}
			else{
				return null;
				//TODO: return the wrong information.
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			//TODO: return the error information
		}
	}
	
	
	public static void main(String[] args){
		try {
			
			DataSet ds = DataSet.loadDataSet("data/nyt2013_ready.zip");
			
//			while (true){
				//BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				//String line = bufferedReader.readLine();
				String line = "CREATE NETWORK ins WHERE Topic.TopicHier = \"Gun Control\"";
				System.out.println(cleanStatement(line));
				Object something = parseStatement(ds, line);
				if (something.getClass().equals(Subnetwork.class)){
					for (NodeType nt : ((Network)something).getNodes().keySet()){
						System.out.println(nt.getName() + "   " +  ((Network)something).getNodes().get(nt).getNodeCount());
//						if (nt.getName().equals("Person") || nt.getName().equals("Topic")){
//							for (Node n : ((Network)something).getNodes().get(nt).getNodes())
//								System.out.println(n.getName());
//						}
					}
				}
				else if (something.getClass().equals(SimilaritySearchAlgoResult.class)){
					for (Node n : ((SimilaritySearchAlgoResult)something).similarityScores.keySet()){
						for (Node neighbor : ((SimilaritySearchAlgoResult)something).similarityScores.get(n).keySet()){
							System.out.println(neighbor.getName() + ":  " + ((SimilaritySearchAlgoResult)something).similarityScores.get(n).get(neighbor));
						}
					}
					
				}
				
				System.out.println("Finish a statement");
//			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}