package OLAP;

public enum OLAPOperationType {
	query, rollup, drilldown
}
