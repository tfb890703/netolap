package network;

import Algorithm.AllAlgorithms;
import Algorithm.BasicAlgorithm;
import algorithm_metric.SavedMetric;
import org.json.JSONArray;
import org.json.JSONObject;

import Algorithm.SimilaritySearchAlgo;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/**
 * Created by george on 4/5/14.
 * Copyright George Brova
 */
public class DataSet {

    RawNetwork rawNetwork;

    //Store the dataset name and description
    String name;
    String desc;

    Map<String, Hierarchy> hierarchyMap;
    NodeType centralNodeType;
    List<NodeType> nodeTypes;
    
    //A map to store subnetworks
    Map<ConstraintSet, Subnetwork> subnetworks = new HashMap<ConstraintSet, Subnetwork>();
    Map<String, Subnetwork> subnetworksByName = new HashMap<String, Subnetwork>();

    AllAlgorithms allAlgorithms;
    Map<SavedMetric, Object> savedMetricsMap = new HashMap<SavedMetric, Object>();
    
    public Object getSavedMetricObject(SavedMetric s) {
		return savedMetricsMap.get(s);
	}

    private void loadMetric(SavedMetric metric, File curFile, DataSet dataSet) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(curFile));
        Object currentMetricObject = metric.loadFromFile(reader, dataSet);
        savedMetricsMap.put(metric, currentMetricObject);
        reader.close();
    }

    private void handleSavedMetric(SavedMetric metric, String metricsPathPrefix) {
        //Adding files to a zip archive is hard. Let's just use a folder for now.
        String filepath = metricsPathPrefix + metric.getMetricName() + ".metric";
        File curFile = new File(filepath);

        if (curFile.exists()) {
            //The current saved metric exists. We need to load it
            try {
                loadMetric(metric, curFile, this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            //The current saved metric doesn't exist. We need to create and load it
            try {
                //Create
                BufferedWriter writer = new BufferedWriter(new FileWriter(curFile));
                metric.saveToFile(writer, this);
                writer.close();

                //Load
                loadMetric(metric, curFile, this);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public void handleSavedMetrics() {
        //Determine which saved metrics are required
        Set<SavedMetric> requiredMetrics = new HashSet<SavedMetric>();
        for (Class<? extends BasicAlgorithm> algo : allAlgorithms.getAlgorithms()) {
            try {
                Set<SavedMetric> currentRequiredMetrics = null;
                currentRequiredMetrics = (Set<SavedMetric>)algo.getDeclaredField("requiredMetrics").get(algo);
                requiredMetrics.addAll(currentRequiredMetrics);
            } catch (Exception e) {
                System.out.println("Warning: Did not get any saved metrics for " + algo.getName());
            }
        }

        System.out.println("Number of metrics to load: " + requiredMetrics.size());

        //Get the metrics path prefix
        if (zipDatasetPath == null) {
            System.err.println("zipDatasetPath is not defined. Exiting");
            return;
        }
        String metricsPathPrefix = zipDatasetPath + "_savedMetrics/";

        //Make the folder if it doesn't exist
        File metricsFolder = new File(metricsPathPrefix);
        if (!metricsFolder.exists()) {
            metricsFolder.mkdir();
        }

        //Check if these metrics exist in the zip. If yes, load them; otherwise, compute and save them.
        for (SavedMetric metric : requiredMetrics) {
            //TODO: ensure all of these metrics have unique names (to help catch bugs)
            handleSavedMetric(metric, metricsPathPrefix);
        }

    }

    public void buildNodes(JSONObject nodeJson, NodeType curNodeType, BufferedReader reader) throws Exception {
        System.out.println("reading " + nodeJson.getString("File"));

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");
            int id = Integer.parseInt(parts[0]);
            String name = parts[1];
            Node curNode = new Node(name, curNodeType, id);
            rawNetwork.addNode(curNode);

            //TODO add any extra attributes

        }
    }


    public void buildRelations(BufferedReader reader) throws Exception {
        System.out.println("Reading relations");

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");
            int fromId = Integer.parseInt(parts[0]);
            int toId   = Integer.parseInt(parts[1]);
            int type   = Integer.parseInt(parts[2]);
            // Undirected
            if (type == 0){
            	rawNetwork.addLink(fromId, toId);
            	rawNetwork.addLink(toId, fromId);
            }else if (type == 1){
            	rawNetwork.addLink(fromId, toId);
            }
        }
    }

    public void buildHierarchy(String hierName, BufferedReader reader) throws Exception{
        System.out.println("Reading hierarchy " + hierName);

        Hierarchy thisHierarchy = new Hierarchy(this, hierName);
        thisHierarchy.readHierFromReader(reader);

        hierarchyMap.put(hierName, thisHierarchy);
    }

    private void buildHierarchyAssociations(BufferedReader reader, String nodeTypeStr, String hierName) throws Exception {
        Hierarchy currentHierarchy = hierarchyMap.get(hierName);
        System.out.println("Reading hierarchy data for " + nodeTypeStr + " and " + hierName);
        NodeType nodeTypeObj = getNodeTypeByName(nodeTypeStr);
        nodeTypeObj.addHierarchy(currentHierarchy);

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split("\t");
            int networkNodeId = Integer.parseInt(parts[0]);
            int hierarchyNodeId = Integer.parseInt(parts[1]);

            Node curNode = rawNetwork.getNodeById(networkNodeId);
            HierarchyNode hNode = currentHierarchy.getHierarchyNodeById(hierarchyNodeId);
            curNode.addHierarchyLink(hNode);
            hNode.addNetworkAssociation(curNode);
        }
    }

    public String getAssociatedText(Node node) {
        String associatedText = "";
        if (ZipDataset == null || associatedTextPath == null) {
            return null;
        }

        try {
            //Prepare reader
            String filepath = associatedTextPath + node.getId() + ".txt";
            if (ZipDataset.getEntry(filepath) == null) {
                return null;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(ZipDataset.getInputStream(ZipDataset.getEntry(filepath))));

            //Read to string
            String line = "";
            while ((line = reader.readLine()) != null) {
                associatedText += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return associatedText;
    }

    private ZipFile ZipDataset = null;
    private String associatedTextPath = null;
    private String zipDatasetPath = null;

    public void buildDataset(String path) {

        this.zipDatasetPath = path;

        try {
            ZipDataset = new ZipFile(path);

            //not sure if we need this (it's a list of entries)
            List entries = Collections.list(ZipDataset.entries());

            //Identify the json file (which should contain the schema)
            String jsonName = "";
            for (Object entry : entries) {
                String filename = ((ZipEntry)entry).getName();
                if (filename.endsWith(".json")) {
                    if (jsonName.length() > 0) {
                        //There are multiple json files; this is confusing, so just exit with an error
                        System.out.println("last: " + jsonName + ", this: " + filename);
                        throw new Exception("There are multiple .json files, it's unclear which one to use. Exiting");
                    }
                    else {
                        jsonName = filename;
                    }
                }
            }

            String filesPrefix = jsonName.substring(0, jsonName.lastIndexOf('/') + 1);
            ZipEntry jsonZipEntry = ZipDataset.getEntry(jsonName);
            if (jsonZipEntry == null) {
                System.err.println("could not find " + jsonName);
                return;
            }

            //read json file
            BufferedReader readJson = new BufferedReader( new InputStreamReader( ZipDataset.getInputStream(jsonZipEntry) ));
            String entireJsonStr = "";
            String line = "";
            while ((line = readJson.readLine()) != null) {
                entireJsonStr += line;
            }
            JSONObject jsonObject = new JSONObject(entireJsonStr);

            //Check that the system name is correct
            if (!"NETOLAP".equals(jsonObject.get("System"))) {
                System.err.println("json file not for NETOLAP system");
                return;
            }

            //Get the dataset name and description
            this.name = jsonObject.getString("DataSet Name");
            this.desc = jsonObject.getString("DataSet Description");
            System.out.println("Parsing dataset: " + this.name);


            //Read node files
            JSONArray nodeFiles = jsonObject.getJSONArray("Node Files");
            for (int id = 0; id < nodeFiles.length(); id++) {
                // parse the schema
                JSONObject nodeJson = nodeFiles.getJSONObject(id);
                String nodeTypeName = nodeJson.getString("Nodetype");
                boolean nodeIsCentral = nodeJson.getBoolean("Central");

                //Add the NodeType
                NodeType curNodeType = new NodeType(nodeTypes.size(), nodeTypeName, nodeIsCentral);
                this.addNodeType(curNodeType);
                if (nodeIsCentral){
                	this.centralNodeType = curNodeType;
                }
                
                //Check attributes
                JSONArray attributesJson = nodeJson.getJSONArray("Attribute");
                if (!attributesJson.getJSONObject(0).getString("Attribute Name").equals("ID")) {
                    System.out.println("Attribute[0] is not ID");
                    return;
                }
                if (!attributesJson.getJSONObject(1).getString("Attribute Name").equals("Name")) {
                    System.out.println("Attribute[1] is not Name");
                    return;
                }
                //TODO: account for other attributes

                //Read the node data
                String filename = nodeJson.getString("File");
                String filepath = filesPrefix + filename;
                BufferedReader reader = new BufferedReader(new InputStreamReader(ZipDataset.getInputStream(ZipDataset.getEntry(filepath))));
                buildNodes(nodeJson, curNodeType, reader);
            }

            //Read relation file
            JSONObject relationJson = jsonObject.getJSONObject("Relation File");
            JSONArray relationTypesJson = relationJson.getJSONArray("Relations");
            //TODO: what do the relation types symbolize?
            String relationFilename = relationJson.getString("File");
            String relationFilepath = filesPrefix + relationFilename;
            BufferedReader relationReader = new BufferedReader(new InputStreamReader(ZipDataset.getInputStream(ZipDataset.getEntry(relationFilepath))));
            buildRelations(relationReader);
            relationReader.close();

            //Read hierarchy files
            JSONArray hierarchyFilesJson = jsonObject.getJSONArray("Hierarchy Files");
            for (int id = 0; id < hierarchyFilesJson.length(); id++) {
                JSONObject hierarchyFileJson = hierarchyFilesJson.getJSONObject(id);
                String hierName = hierarchyFileJson.getString("Name");
                String hierFilename = hierarchyFileJson.getString("File");
                String hierFilepath = filesPrefix + hierFilename;
                BufferedReader hierReader = new BufferedReader(new InputStreamReader(ZipDataset.getInputStream(ZipDataset.getEntry(hierFilepath))));
                buildHierarchy(hierName, hierReader);
            }

            //Read hierarchy mapping files
            JSONArray hierarchyMappingFilesJson = jsonObject.getJSONArray("Hierarchy Mapping Files");
            for (int id = 0; id < hierarchyMappingFilesJson.length(); id++) {
                JSONObject hierarchyMappingFileJson = hierarchyMappingFilesJson.getJSONObject(id);
                String nodetype = hierarchyMappingFileJson.getString("Nodetype");
                String hierName = hierarchyMappingFileJson.getString("Hierarchy");
                String mapFilename = hierarchyMappingFileJson.getString("File");
                String mapFilepath = filesPrefix + mapFilename;
                BufferedReader hierReader = new BufferedReader(new InputStreamReader(ZipDataset.getInputStream(ZipDataset.getEntry(mapFilepath))));

                buildHierarchyAssociations(hierReader, nodetype, hierName);
            }

            //Read nodes' associated text path
            String associatedTextPathLabel = "Associated Text Path";
            if (jsonObject.has(associatedTextPathLabel)) {
                this.associatedTextPath = filesPrefix + jsonObject.getString(associatedTextPathLabel);
            }


        }
        catch (Exception e) {
            e.printStackTrace();
        }


        //Other things to do:
        //also write a method to serialize a network into a file
        //write a deserializer

        //remember empty nodes in the hierarchy too.
    }
    

    public DataSet() {
        nodeTypes = new ArrayList<NodeType>();
        rawNetwork = new RawNetwork(this);
        hierarchyMap = new HashMap<String, Hierarchy>();
        allAlgorithms = new AllAlgorithms();
    }

  

    private void addNodeType(NodeType nodeType) {
        nodeTypes.add(nodeType);
    }


    public RawNetwork getRawNetwork() {
		return rawNetwork;
	}
    
    public List<NodeType> getNodeTypes() {
		return nodeTypes;
	}
    
    public NodeType getNodeTypeByName(String typeName) throws Exception{
    	for (NodeType nodeType : nodeTypes){
    		if (nodeType.name.equals(typeName))
    			return nodeType;
    	}
    	
    	throw new Exception("No node type " + typeName + " in the dataset.");
    }
    
    public NodeType getCentralNodeType() {
		return centralNodeType;
	}
    
    public String getName() {
		return name;
	}
    
    
    /**
     * Get a subnetwork by giving a constraint set
     * @param consSet: constraint set
     * @return The same constraint set.
     */
    public Subnetwork getSubnetworkByConstraints(ConstraintSet consSet){
    	for (ConstraintSet tempSet : this.subnetworks.keySet()){
    		if (ConstraintSet.compare(tempSet, consSet) == ContainType.equal){
    			return this.subnetworks.get(tempSet);
    		}
    	}
    	return null;
    }
    
    /**
     * Get a subnetwork by giving a network name
     * @param name
     * @return If the name is not saved before, throw error.
     */
    public Subnetwork getSubnetworkByName(String name){
    	for (String netName : this.subnetworksByName.keySet())
    	{
    		if (netName.equals(name)){
    			return this.subnetworksByName.get(netName);
    		}
    	}
    	return null;
    }
    
    /**
     * Save a subnetwork by giving a constraint list
     * @param subnetwork
     * @param constraints
     */
    public void saveSubnetwork(Subnetwork subnetwork, ConstraintSet consSet){
    	this.subnetworks.put(consSet, subnetwork);
    	String netName = subnetwork.getName();
    	this.subnetworksByName.put(netName, subnetwork);
    }
    
    public void saveSubnetworkByName(Subnetwork subnetwork, String name){
    	this.subnetworksByName.put(name, subnetwork);
    }
    
   
    public static DataSet loadDataSet(String zipFile){
    	
    	DataSet d = new DataSet();
    	d.zipDatasetPath = zipFile;
    	d.buildDataset(zipFile);
    	d.handleSavedMetrics();
    	
    	return d;
    }
    
    // For unit test
    public static void main(String[] args) {
        //String path = "data/four_area_new.zip";
        String path = "data/nyt2013_ready.zip";
        DataSet d = new DataSet();
        d.zipDatasetPath = path;
        d.buildDataset(path);

        //SimilaritySearchAlgo simiAlgo = new SimilaritySearchAlgo(null);
        //System.out.println( d.getAssociatedText(d.getRawNetwork().getNodeById(42138)) );

        d.handleSavedMetrics();
    }
    
    public static DataSet buildDatasetTest(){
    	String path = "data/four_area_new.zip";
        DataSet d = new DataSet();
        d.buildDataset(path);
        System.out.print("as");
        
        return d;
        
        //SimilaritySearchAlgo simiAlgo = new SimilaritySearchAlgo(null);
        
    }
}
