package network;

import java.util.ArrayList;

/**
 * Created by george on 4/5/14.
 * Copyright George Brova
 */
public class Constraint {	
	private NodeType nodeType;
	private ConstraintType constraintType;
	private ArrayList<HierarchyNode> hierarchyNodes;
	
	public ConstraintType getConstraintType() {
		return constraintType;
	}
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public ArrayList<HierarchyNode> getHierarchyNodes() {
		return hierarchyNodes;
	}
	
	public void setConstraintType(ConstraintType constraintType) {
		this.constraintType = constraintType;
	}
	
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	public void addHierarchyNode(HierarchyNode node){
		this.hierarchyNodes.add(node);
	}
	
	public Constraint(){
		this.hierarchyNodes = new ArrayList<HierarchyNode>();
	}
}
