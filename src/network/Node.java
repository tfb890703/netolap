package network;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by george on 4/2/14.
 * Copyright George Brova
 */
public class Node {

    private String name;
    private NodeType type;
    private int id;
    
    public HashMap<Hierarchy, HashSet<HierarchyNode>> hierarchyLinks;
    
    public NodeType getType() {
		return type;
	}
    
    public String getName() {
		return name;
	}
    
    public HashMap<Hierarchy, HashSet<HierarchyNode>> getHierarchyLinks() {
		return hierarchyLinks;
	}

    public void addHierarchyLink(HierarchyNode hierarchyNode) {
        Hierarchy hierarchy = hierarchyNode.getHierarchy();
        if (!hierarchyLinks.containsKey(hierarchy)) {
            hierarchyLinks.put(hierarchy, new HashSet<HierarchyNode>());
        }
        hierarchyLinks.get(hierarchy).add(hierarchyNode);
    }

    public int getId() {
		return id;
	}
    
    public Node(String name, NodeType type, int id){
    	this.name = name;
    	this.type = type;
    	this.id = id;
    	this.hierarchyLinks = new HashMap<Hierarchy, HashSet<HierarchyNode>>();
    }
    
    



}
