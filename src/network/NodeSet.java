package network;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by george on 4/5/14.
 * Copyright George Brova
 */
public class NodeSet {
	NodeType nodeType;
	Set<Node> nodes;
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public Set<Node> getNodes() {
		return nodes;
	}
	
	public Node getNodeByName(String name) throws Exception{
		for (Node n : nodes){
			if (n.getName().equals(name))
				return n;
		}
		throw new Exception("No node named " + name + " found in the subnetwork");
	}
	
	public int getNodeCount(){
		return nodes.size();
	}
	
	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = new HashSet<Node>(nodes);
	}
	
	public NodeSet(NodeType nodeType){
		this.nodeType = nodeType;
		this.nodes = new HashSet<Node>();
	}
	
	
	/**
	 * Add one node into the node set
	 * TODO: Check if there is duplicated node
	 * @param node
	 */
	public void addNode(Node node){
		this.nodes.add(node);
	}
	
	/**
	 * Add a set of node into the node set
	 * TODO: Check if there is duplicated node
	 * @param newNodes
	 */
	public void addNodes(List<Node> newNodes){
		this.nodes.addAll(newNodes);
	}
	
	public void removeNode(Node node){
		this.nodes.remove(node);
	}
	
	public Boolean containNode(Node node){
		if (node.getType() != this.nodeType)
			return false;
		
		if (this.nodes.contains(node))
			return true;
		else
			return false;
	}
	
}
