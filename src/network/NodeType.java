package network;

import java.util.ArrayList;

/**
 * Created by george on 4/5/14.
 * Copyright George Brova
 */
public class NodeType {

    int type;
    String name;		// Type name
    boolean central;	// If the node type is central or rich-text.
    
    ArrayList<Hierarchy> hierarchies;
    
    
    public int getType() {
		return type;
	}
    
    public String getName() {
		return name;
	}
    
    public boolean isCentral(){
    	return central;
    }
    
    
    public ArrayList<Hierarchy> getHierarchies() {
		return hierarchies;
	}
    
    public Hierarchy getHierarchyByName(String hierName) throws Exception{
    	for (Hierarchy hier : this.hierarchies)
    		if (hier.getHierarchyName().equals(hierName)){
    			return hier;
    		}
    	
    	throw new Exception("Node type " + this.name + " has no hierarchy " + hierName + "."); 
    }
    
    public void addHierarchy(Hierarchy hierarchy){
    	this.hierarchies.add(hierarchy);
    }
    
    
    public NodeType(int type, String name, boolean central){
    	this.type = type;
    	this.name = name;
    	this.central = central;
    	this.hierarchies = new ArrayList<Hierarchy>();
    }
}
