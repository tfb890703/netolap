package network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by george on 4/5/14.
 * Copyright George Brova
 */
public class Subnetwork extends Network {
	
	String name;

		
	//TODO: Design a structure to store the aggregation metrics.
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Subnetwork(DataSet ds){
		this(ds, "Unknown");
	}
	
	public Subnetwork(DataSet ds, String name) {
		super(ds);
		this.name = name;
	}
	
	
	
	/**
     * Establish links for the network based on 
     */
    public void buildLinks(){
    	RawNetwork rawNetwork = this.ds.getRawNetwork();
    	
    	for (NodeType nType : this.nodes.keySet()){
    		for (Node node : this.nodes.get(nType).getNodes()){
    			if (this.links.containsKey(node))
    				continue;
    			
    			//Build links for this node
    			Map<NodeType, ArrayList<Node>> linksForOne = new HashMap<NodeType, ArrayList<Node>>();
    			for (NodeType type : this.ds.getNodeTypes()){
    				ArrayList<Node> newNeighbors = new ArrayList<Node>();
    				NodeSet neighborDataSet = this.nodes.get(type);
    				for (Node neighborNode : rawNetwork.getLinksByNodeAndType(node, type)){
    					if (neighborDataSet.containNode(neighborNode)){
    						newNeighbors.add(neighborNode);
    					}
    				}
    				linksForOne.put(type, newNeighbors);
    			}
    			this.links.put(node, linksForOne);
    		}
    	}
    	
    }
	
}
