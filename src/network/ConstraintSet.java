package network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Fangbo Tao
 *
 */
public class ConstraintSet {
	
	private DataSet ds;
	private Set<Constraint> constraints;					//A list of constraints
	private Map<NodeType, Map<Hierarchy, Set<HierarchyNode>>> simplified;		//Simplified constraints, use this for the comparison between constraintSets
	
	
	public ConstraintSet(DataSet ds) {
		this.ds = ds;
		constraints = new HashSet<Constraint>();
		simplified = new HashMap<NodeType, Map<Hierarchy,Set<HierarchyNode>>>();
		
		//Initialize the simplified 
		for (NodeType nt : this.ds.getNodeTypes()){
			simplified.put(nt, new HashMap<Hierarchy, Set<HierarchyNode>>());
			for (Hierarchy hier : nt.getHierarchies()){
				simplified.get(nt).put(hier, new HashSet<HierarchyNode>());
			}
		}
	}
	
	public Set<Constraint> getConstraints() {
		return constraints;
	}
	
	public Map<NodeType, Map<Hierarchy, Set<HierarchyNode>>> getSimplified() {
		return simplified;
	}
	
	
	/**
	 * Convert the constraints to non-duplicated set
	 */
	public void simplifyConstraints(){
		
		//Combine different set
		for (Constraint cons : this.constraints){
			NodeType nt = cons.getNodeType();
			for (HierarchyNode hNode : cons.getHierarchyNodes()){
				Hierarchy hierarchy = hNode.getHierarchy();
				simplified.get(nt).get(hierarchy).add(hNode);
			}
		}
		
		//Simplify the nodes
		for (NodeType nt : simplified.keySet()){
			for (Hierarchy hierarchy : simplified.get(nt).keySet()){
				HashSet<HierarchyNode> simpleNodeSet = new HashSet<HierarchyNode>(Hierarchy.getMinimalSet(simplified.get(nt).get(hierarchy)));
				simplified.get(nt).put(hierarchy, simpleNodeSet); 
			}
		}
	}
	
	public void addConstraint(Constraint cons){
		this.constraints.add(cons);
	}
	
	public void addContraints(ArrayList<Constraint> newConstraints){
		this.constraints.addAll(newConstraints);
	}
	
	/**
	 * compare two different constraint set and 
	 * @param setA
	 * @param setB
	 * @return Contain type.
	 */
	public static ContainType compare(ConstraintSet setA, ConstraintSet setB){
		
		List<ContainType> result = new ArrayList<ContainType>();
		for (NodeType nt : setA.getSimplified().keySet()){
			for (Hierarchy hierarchy : setA.getSimplified().get(nt).keySet()){
				result.add(Hierarchy.compareNodeSet(setA.getSimplified().get(nt).get(hierarchy), 
						setB.getSimplified().get(nt).get(hierarchy)));
			}
		}
		
		if ((result.contains(ContainType.leftContaining) && result.contains(ContainType.rightContaining)) || (result.contains(ContainType.exclusive)))
			return ContainType.exclusive;
		
		if (result.contains(ContainType.leftContaining))
			return ContainType.leftContaining;
		
		if (result.contains(ContainType.rightContaining))
			return ContainType.rightContaining;
		
		return ContainType.equal;
		
	}
	
}
