package network;

/**
 * A class to specify constraint types. We only support hierarchy in our first version.
 * @author taofangbo
 */
public enum ConstraintType {
	hierarchy, attribute
}