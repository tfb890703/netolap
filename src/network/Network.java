package network;

import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by george on 4/2/14.
 * Copyright George Brova, Fangbo Tao
 */
public abstract class Network {

	//Data section
	//We want to store the network in a proper structure such that our operations can be done efficiently
	DataSet ds;
	Map<NodeType, NodeSet> nodes = new HashMap<NodeType, NodeSet>();
	ArrayList<Node> allNodes;
    Map<Integer, Node> nodesByID = new HashMap<Integer, Node>();
    Map<Node, Map<NodeType, ArrayList<Node>>> links = new HashMap<Node, Map<NodeType,ArrayList<Node>>>();
	
	public DataSet getDs() {
		return ds;
	}
    
    public Map<NodeType, NodeSet> getNodes() {
		return nodes;
	}
	
    public Node getNodeById(int nodeid) {
        return nodesByID.get(nodeid);
    }
    
	public NodeSet getNodesByType(NodeType type){
		return nodes.get(type);
	}
	
	public Node getNodeByTypeAndName(NodeType type, String name) throws Exception{
		NodeSet nodeSet = nodes.get(type);
		return nodeSet.getNodeByName(name);
	}
	
	public Map<Node, Map<NodeType, ArrayList<Node>>> getLinks() {
		return links;
	}
	
	
	public Map<NodeType, ArrayList<Node>> getLinksByNode(Node node) {
		return links.get(node);
	}
	
	public ArrayList<Node> getAllLinksByNode(Node node) {
		Map<NodeType, ArrayList<Node>> nodeLinks = links.get(node);
		ArrayList<Node> resultArrayList = new ArrayList<Node>();
		for (NodeType nt : nodeLinks.keySet())
			resultArrayList.addAll(nodeLinks.get(nt));
		return resultArrayList;
	}
	
	public ArrayList<Node> getAllNodes() {
		if (allNodes == null){
			allNodes = new ArrayList<Node>();
			for (NodeType nt : nodes.keySet()){
				allNodes.addAll(nodes.get(nt).nodes);
			}
			return allNodes;
		}
		else{
			return allNodes;
		}
	}
	
	
	public ArrayList<Node> getLinksByNodeAndType(Node node, NodeType type) {
		try {
			ArrayList<Node> result = links.get(node).get(type);
			if (result == null)
				return new ArrayList<Node>();
			else
				return result;
		} catch (Exception e) {
			return new ArrayList<Node>();
		}
		
	}
	
	
	/***
	 * Add a node list into the network, only allows one node type
	 * @param nType Node type
	 * @param nodes Added node list
	 */
	public void addNodes(NodeType nType, List<Node> nodes){
		this.nodes.get(nType).addNodes(nodes);
	}
	
	
	
	public Network(DataSet ds){
		this.ds = ds;
		//Set up node lists
		for (NodeType nType : ds.getNodeTypes()){
			this.nodes.put(nType, new NodeSet(nType));
		}
	}


    public void addNode(Node n) throws Exception {
        //add to nodesById
        if (nodesByID.containsKey(n.getId())) {
            throw new Exception("Node ids are not unique. Repeated id found: " + n.getId());
        }
        nodesByID.put(n.getId(), n);

        //add to nodes
        if(!nodes.containsKey(n.getType())) {
            nodes.put(n.getType(), new NodeSet(n.getType()));
        }
        nodes.get(n.getType()).addNode(n);
    }

    

    /**
     * 
     * @param fromId
     * @param toId
     */
    public void addLink(int fromId, int toId) {
        //TODO implement
        Node fromNode = nodesByID.get(fromId);
        Node toNode = nodesByID.get(toId);
        

        //check against nulls
        if (!links.containsKey(fromNode)) {
            links.put(fromNode, new HashMap<NodeType,ArrayList<Node>>() );
        }
        Map<NodeType, ArrayList<Node>> fromNodeLinks = links.get(fromNode);
        
        
        try {
        	if (!fromNodeLinks.containsKey(toNode.getType())) {
                fromNodeLinks.put(toNode.getType(), new ArrayList<Node>());
            }
		} catch (Exception e) {
			System.out.print("asd");
		}
        
        
        

        //add the edge
        fromNodeLinks.get(toNode.getType()).add(toNode);
    }

    //TODO: implement these
    public void addNode(){
        //What properties do we need? For example, id, label, type
    }

    public void removeNode() {

    }
    
    
    

   

    
    /***
     * Get a NodeSet by a constraint.
     * @param constraint A constraint on one type of node
     * @return a NodeSet
     */
    public NodeSet getNodes(Constraint constraint) {
    	NodeSet resultSet = new NodeSet(constraint.getNodeType());
    	if (constraint.getConstraintType() == ConstraintType.hierarchy)
    	{
    		ArrayList<Node> tempNodes = new ArrayList<Node>();

			// Traverse all the nodes
			Set<HierarchyNode> hierNodeQueue = new HashSet<HierarchyNode>(constraint.getHierarchyNodes());
			while (!hierNodeQueue.isEmpty())
			{
				HierarchyNode firstChild = hierNodeQueue.iterator().next();
				// In case it doesn't contain any network nodes:
				if (firstChild.getNetworkNodes().containsKey(constraint.getNodeType())){
					tempNodes.addAll(firstChild.getNetworkNodes().get(constraint.getNodeType()));
				}
				hierNodeQueue.remove(firstChild);
				hierNodeQueue.addAll(firstChild.getChildren().values());
			}
    		
			// Remove the duplicated nodes
    		tempNodes = new ArrayList<Node>(new HashSet<Node>(tempNodes));
    		resultSet.setNodes(tempNodes);
    	}
    	else{
    		//TODO: If the constraint type is attribute
    	}
    	
        return resultSet;
    }

    
    /**
     * Deprecated. Generate a subnetwork based on a set of central nodes
     * @param nodes
     * @return
     */
    public Network getSubnetwork(Set<Node> nodes) {
        return null;
    }
    
    

    /***
     * Build a nodeSets based on a list of node sets, DO NOT include the central node sets
     * Important!! This implementation only works for star schema !!!
     * TODO: Make it more general
     * @param nodeSets: node sets for different node types.
     * @return A sub-network
     */
    public Subnetwork buildSubnetwork(List<NodeSet> nodeSets) {
    	
    	//Initailize subnetwork
    	Subnetwork subnetwork = new Subnetwork(ds);
    	
    	
    	//Find the central nodes given different node sets
    	Boolean initail = true;
    	ArrayList<Node> centralNodes = new ArrayList<Node>();
    	HashMap<NodeType, NodeSet> constrainedNodeTypes = new HashMap<NodeType, NodeSet>();
    	for (NodeSet ns : nodeSets){
    		ArrayList<Node> tempNodes = new ArrayList<Node>(); 
    		for (Node node : ns.getNodes()){
    			tempNodes.addAll(this.getLinksByNodeAndType(node, ds.getCentralNodeType()));
    		}
    		if (initail == true){
    			centralNodes = tempNodes;
    		}
    		else {
				centralNodes.retainAll(tempNodes);
			}
    		initail = false;
    		
    		constrainedNodeTypes.put(ns.getNodeType(), ns);
    	}
    	
    	//Add other node types which are not in the constrained node sets. Expand them using the central nodes
    	for (NodeType nType : this.ds.getNodeTypes())
    	{
    		if (nType.isCentral()){
    			subnetwork.addNodes(nType, centralNodes);
    			continue;
    		}
    			
    		//For node type that already be constrained
    		if (constrainedNodeTypes.containsKey(nType)){
    			ArrayList<Node> tempNodes = new ArrayList<Node>();
    			for (Node node : centralNodes){
    				tempNodes.addAll(this.getLinksByNodeAndType(node, nType));
    			}
    			
    			//Only remain the constrained nodes
    			tempNodes.retainAll(constrainedNodeTypes.get(nType).getNodes());
    			subnetwork.addNodes(nType, tempNodes);
    		}
    		//For node type that has no constaints
    		else{
    			ArrayList<Node> tempNodes = new ArrayList<Node>();
    			for (Node node : centralNodes){
    				tempNodes.addAll(this.getLinksByNodeAndType(node, nType));
    			}
    			
    			//Put the node set into the network, keep all the nodes that is connected to the central node set
    			subnetwork.addNodes(nType, tempNodes);
    		}
    	}
    	
    	
    	//Filter the links for the sub-network
    	subnetwork.buildLinks();
    	
    	return subnetwork;
    }
}
