package network;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;


/**
 * Created by george on 4/2/14.
 * Copyright George Brova
 */
public class Hierarchy {
	
    private HierarchyNode root;
    private Map<String, HierarchyNode> nodesMap = new HashMap<String, HierarchyNode>();
    private Map<Integer, HierarchyNode> nodesMapById = new HashMap<Integer, HierarchyNode>();
    DataSet dataset;

    String hierarchyName = "";
    
    public Hierarchy(DataSet ds, String name) {
    	this.hierarchyName = name;
    	this.dataset = ds;
        //Set up root
        root = new HierarchyNode(this, 0, null, "all");
        nodesMap.put(root.name, root);
        nodesMapById.put(root.id, root);
    }
    
    
    public String getHierarchyName() {
		return hierarchyName;
	}
    
    public DataSet getDataset() {
		return dataset;
	}

    public void addNode(int node_id, int parent_id, String name) {
        if (nodesMapById.containsKey(node_id) && nodesMapById.get(node_id).parent != null) {
            //This node already exists; throw an error
            throw new Error("Error: node with id="+node_id+" already exists");
        }

        //Make a temporary parent node
        HierarchyNode parent_node = null;
        if (nodesMapById.containsKey(parent_id)) {
            parent_node = nodesMapById.get(parent_id);
        }
        else {
            //Make temporary parent node, and add it to the list of nodes
            parent_node = new HierarchyNode(this, parent_id, null, null);
            nodesMapById.put(parent_node.id, parent_node);
        }

        //Make current node if it doesn't exist (ie if we didn't add it as a parent before)
        HierarchyNode curNode = null;
        if (nodesMapById.containsKey(node_id)) {
            curNode = nodesMapById.get(node_id);
            curNode.parent = parent_node;
            curNode.name = name;
        }
        else {
            curNode = new HierarchyNode(this, node_id, parent_node, name);
        }

        //Build child link
        parent_node.addChild(curNode);

        //Add to list of nodes
        nodesMap.put(curNode.name, curNode);
        nodesMapById.put(curNode.id, curNode);

    }
    
    public HierarchyNode getHierarchyNodeByName(String name) throws Exception{
    	if (!nodesMap.containsKey(name))
    		throw new Exception("Hierarchy " + this.hierarchyName + " does not contain node " + name + ".");
    	return nodesMap.get(name);
    }
    public HierarchyNode getHierarchyNodeById(int id) throws Exception{
        if (!nodesMapById.containsKey(id))
            throw new Exception("Hierarchy " + this.hierarchyName + " does not contain node " + id + ".");
        return nodesMapById.get(id);
    }
    
    
    /**
     * Minimize the representative hierarchy nodes
     * @param hierarchyNodes A set of hierarchy node
     * @return A minimized list
     */
    public static ArrayList<HierarchyNode> getMinimalSet(Set<HierarchyNode> hierarchyNodes){
    	ArrayList<HierarchyNode> result = new ArrayList<HierarchyNode>(hierarchyNodes);
    	
    	for (HierarchyNode hNode : hierarchyNodes){
    		HierarchyNode temp = hNode;
    		while (temp.parent != null){
    			temp = temp.parent;
    			if (hierarchyNodes.contains(temp)){
    				result.remove(hNode);
    				break;
    			}
    		}
    	}
    	return result;
    }
    
    /**
     * The two set should already be minimized before this function is called.
     * @param setA
     * @param setB
     * @return The contain type
     */
    public static ContainType compareNodeSet(Set<HierarchyNode> setA, Set<HierarchyNode> setB){
    	
    	if (setA.equals(setB))
    		return ContainType.equal;
    	
    	
    	boolean AContainB = true;
    	//Test if A contains B
    	for (HierarchyNode hNode : setB){
    		HierarchyNode temp = hNode;
    		while (temp.parent != null){
    			if (setA.contains(temp)){
    				break;
    			}
    			temp = temp.parent;
    		}
    		
    		AContainB = false;
    		break;
    	}
    	if (AContainB){
    		return ContainType.leftContaining;
    	}
    	
    	boolean BContainA = true;
    	//Test if B contains A
    	for (HierarchyNode hNode : setA){
    		HierarchyNode temp = hNode;
    		while (temp.parent != null){
    			if (setB.contains(temp)){
    				break;
    			}
    			temp = temp.parent;
    		}
    		
    		BContainA = false;
    		break;
    	}
    	if (BContainA){
    		return ContainType.rightContaining;
    	}
    	
    	return ContainType.exclusive;
    	
    }
    
    

    public String toString() {
        return root.toString();
    }

    
    public static void main(String[] args) {
        Hierarchy h = new Hierarchy(null, "test");
        //h.readHierFile("testHierarchy.hier");
        h.readHierFile("data/lochier.hier");
        System.out.println(h);
    }

    public void readHierFile(String filepath) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filepath));
            readHierFromReader(br);
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readHierFromReader(BufferedReader br) throws IOException{
        String line;
        while ((line = br.readLine()) != null) {
            String[] parts = line.split("\t");

            //Check that the line is formatted correctly
            if (parts.length != 3) {
                System.err.println("Warning: skipping misformatted line");
                continue;
            }

            //Add the current node
            int node_id     = Integer.parseInt(parts[0]);
            int parent_id   = Integer.parseInt(parts[1]);
            String name_str = parts[2];
            this.addNode(node_id, parent_id, name_str);
        }

    }
}

final class HierarchyNode {
    int id;
    private Hierarchy hierarchy;
    HierarchyNode parent;
    String name;
    Map<Integer, HierarchyNode> children = new HashMap<Integer, HierarchyNode>();
    Map<NodeType, ArrayList<Node>> networkNodes = new HashMap<NodeType, ArrayList<Node>>();
    
    public Map<NodeType, ArrayList<Node>> getNetworkNodes() {
		return networkNodes;
	}
    
    public Map<Integer, HierarchyNode> getChildren() {
		return children;
	}
    
    public Hierarchy getHierarchy() {
		return hierarchy;
	}

    public HierarchyNode(Hierarchy hierarchy, int id, HierarchyNode parent, String name) {
    	this.hierarchy = hierarchy;
        this.id = id;
        this.parent = parent;
        this.name = name;
    }

    void addChild(HierarchyNode child) {
        children.put(child.id, child);
    }
    
    /**
     * Put a node into the hierarchy map
     * @param node
     */
    public void addNetworkAssociation(Node node){
    	NodeType nt = node.getType();
    	if (!networkNodes.containsKey(nt))
    		networkNodes.put(nt, new ArrayList<Node>());
    	networkNodes.get(nt).add(node);
    }

    // pre-order traversal
    public String toString() {
        String as_str = "[" + this.id + "," + this.name + "]";
        if (children.size() > 0) {
            as_str += ": {";
            for (HierarchyNode n : children.values()) {
                as_str += n.toString();
            }
            as_str += "} ";
        }
        return as_str;
    }

}
