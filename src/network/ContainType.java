package network;

public enum ContainType {
	equal,					// the two sets are totally equal
	exclusive,				// the two sets are exclusive
	leftContaining,			// the left one contains the right one
	rightContaining			// the right one contains the left one
}
