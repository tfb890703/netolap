package network;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

public class Utils {

	public static <K,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return -o1.getValue().compareTo(o2.getValue());
            }
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
      
        for(Map.Entry<K,V> entry: entries){
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }
	
	public static HashMap<String, String> getNode(Node targetNode){
		HashMap<String, String> returnMap = new HashMap<String, String>();
		returnMap.put("name", targetNode.getName());
		returnMap.put("image", "/static/entityImage/" + String.valueOf(targetNode.getId()) + ".png");
		returnMap.put("id", String.valueOf(targetNode.getId()));
		returnMap.put("url", "/entity/" + String.valueOf(targetNode.getId()));
		
		return returnMap;
	}

	
	public static void createJsonForSubnetwork(Subnetwork subnetwork, int count) throws Exception{
		HashMap<String, String> colors = new HashMap<String, String>();
		colors.put("Article", "#ccc");
		colors.put("Person", "#aa0000");
		colors.put("Organization", "#0000FF");
		colors.put("Topic", "#8A008A");
		colors.put("Location", "#00bb00");
		
		HashMap<String, String> edgeColors = new HashMap<String, String>();
		edgeColors.put("Article", "#FFFFFF");
		edgeColors.put("Person", "#550000");
		edgeColors.put("Organization", "#000055");
		edgeColors.put("Topic", "#8A008A");
		edgeColors.put("Location", "#005500");
		Random random = new Random();
		
		JSONObject graphJson = new JSONObject();
		JSONArray nodeArray = new JSONArray();
		JSONArray edgeArray = new JSONArray();
		JSONArray newEdgeArray = new JSONArray();
		
		int index = 0;
		int newIndex = 0;
		
		for (Node node : subnetwork.getAllNodes()){
			if (!node.getType().getName().equals("Article")){
				if (subnetwork.getAllLinksByNode(node).size() < 3)
					continue;
			}
			else{
				continue;
			}
			String color = colors.get(node.getType().getName());
			String label = node.getName();
			
			double size = Math.log((double)subnetwork.getAllLinksByNode(node).size() + 1.5) / Math.log(1.5);
			double x = random.nextDouble();
			double y = random.nextDouble();
			
			if (node.getType().getName().equals("Article")){
				label = "";
				x = x / 3 + 0.33;
				y = y / 3 + 0.33;
			}
			if (node.getType().getName().equals("Person")){
				x = x / 2;
				y = y / 2;
			}
			if (node.getType().getName().equals("Organization")){
				x = x / 2 + 0.5;
				y = y / 2;
			}
			if (node.getType().getName().equals("Location")){
				x = x / 2;
				y = y / 2 + 0.5;
			}
			if (node.getType().getName().equals("Topic")){
				x = x / 2 + 0.5;
				y = y / 2 + 0.5;
			}
			y *= 0.6;
			
			
			
			
			JSONObject nodeJson = new JSONObject();
			
			
			nodeJson.put("id", Integer.toString(node.getId()));
			nodeJson.put("label", label);
			nodeJson.put("x", x);
			nodeJson.put("y", y);
			nodeJson.put("size", size);
			nodeJson.put("color", color);
			nodeArray.put(nodeJson);
			
			//Add edges
			if (node.getType().getName().equals("Article")){
				continue;
			}
			ArrayList<Node> neighbors = subnetwork.getAllLinksByNode(node);
			
			for (Node neighbor : neighbors){
				index += 1;
				JSONObject edgeJson = new JSONObject();
				edgeJson.put("id", "e" + index);
				edgeJson.put("source", Integer.toString(node.getId()));
				edgeJson.put("target", Integer.toString(neighbor.getId()));
				edgeJson.put("color", edgeColors.get(node.getType().getName()));
				edgeArray.put(edgeJson);
			}
			
			//Add direct links
			HashMap<Node, Integer> undirectNeighbors = new HashMap<Node, Integer>();
			for (Node article : neighbors){
				ArrayList<Node> nnbors = subnetwork.getAllLinksByNode(article);
				for (Node nnbor : nnbors){
					if (undirectNeighbors.containsKey(nnbor)){
						undirectNeighbors.put(nnbor, undirectNeighbors.get(nnbor) + 1);
					}
					else{
						undirectNeighbors.put(nnbor, 1);
					}
				}
			}
			
			
			for (Node nnbor : undirectNeighbors.keySet()){
				if (nnbor == node){
					continue;
				}
				
				if (!nnbor.getType().getName().equals("Article")){
					if (subnetwork.getAllLinksByNode(nnbor).size() < 3)
						continue;
				}
				
				if (undirectNeighbors.get(nnbor) < 3){
					continue;
				}
				
				newIndex += 1;
				JSONObject edgeJson = new JSONObject();
				edgeJson.put("id", "e" + newIndex);
				edgeJson.put("source", Integer.toString(node.getId()));
				edgeJson.put("target", Integer.toString(nnbor.getId()));
				edgeJson.put("color", edgeColors.get(node.getType().getName()));
				edgeJson.put("size", undirectNeighbors.get(nnbor));
				newEdgeArray.put(edgeJson);
			}
		}
		
		
		
		
		
		
		
		graphJson.put("nodes", nodeArray);
		graphJson.put("edges", newEdgeArray);
		
		//output to a file
		String jsonString = graphJson.toString();
//		System.out.print(jsonString);	
		PrintWriter out = new PrintWriter("netolap_frontend/netolap_frontend/static/graphs/" + count + ".json");
		out.write(jsonString);
		out.close();
	}
	
}
