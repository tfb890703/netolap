package HttpServer;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;



import network.DataSet;
import network.Node;
import network.Utils;

import com.google.gson.GsonBuilder;


public class EntityHandler extends JSONHandler  {
	
	DataSet ds;
	
	public EntityHandler(DataSet ds){
		this.ds = ds;
		// We do not want to return all the node fields to client
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}
	
	/* (non-Javadoc)
	 * @see HttpServer.JSONHandler#processRequest(java.util.Map)
	 */
	@Override
	public Object processRequest(Map<String, Object> params) {
		System.out.println("Start entity page");
		int eid = Integer.parseInt((String)params.get("id"));
		
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		
		Node targetNode = ds.getRawNetwork().getNodeById(eid);
		
		//Person information:
		returnMap.putAll(Utils.getNode(targetNode));
		
		//Top related entities
		ArrayList<Node> neighbors = ds.getRawNetwork().getAllLinksByNode(targetNode);
		HashMap<Node, Integer> undirectNeighbors = new HashMap<Node, Integer>();
		for (Node article : neighbors){
			ArrayList<Node> nnbors = ds.getRawNetwork().getAllLinksByNode(article);
			for (Node nnbor : nnbors){
				if (undirectNeighbors.containsKey(nnbor)){
					undirectNeighbors.put(nnbor, undirectNeighbors.get(nnbor) + 1);
				}
				else{
					undirectNeighbors.put(nnbor, 1);
				}
			}
		}
		
		HashMap<Node, Integer> perMap = new HashMap<Node, Integer>();
		HashMap<Node, Integer> locMap = new HashMap<Node, Integer>();
		HashMap<Node, Integer> orgMap = new HashMap<Node, Integer>();
		for (Node nnbor : undirectNeighbors.keySet()){
			if (nnbor == targetNode){
				continue;
			}
			
			if (nnbor.getType().getName().equals("Person")){
				perMap.put(nnbor, undirectNeighbors.get(nnbor));
			}
			if (nnbor.getType().getName().equals("Location")){
				locMap.put(nnbor, undirectNeighbors.get(nnbor));
			}
			if (nnbor.getType().getName().equals("Organization")){
				orgMap.put(nnbor, undirectNeighbors.get(nnbor));
			}
		}
		
		perMap = (LinkedHashMap<Node, Integer>)Utils.sortByValues(perMap);
		locMap = (LinkedHashMap<Node, Integer>)Utils.sortByValues(locMap);
		orgMap = (LinkedHashMap<Node, Integer>)Utils.sortByValues(orgMap);
		
		ArrayList<HashMap<String, String>> perList = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> locList = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> orgList = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> articleList = new ArrayList<HashMap<String,String>>();
		
		int counter = 0;
		for (Node node : perMap.keySet()){
			counter += 1;
			if (counter > 5)
				break;
			perList.add(Utils.getNode(node));
		}
		
		counter = 0;
		for (Node node : locMap.keySet()){
			counter += 1;
			if (counter > 5)
				break;
			locList.add(Utils.getNode(node));
		}
		
		counter = 0;
		for (Node node : orgMap.keySet()){
			counter += 1;
			if (counter > 5)
				break;
			orgList.add(Utils.getNode(node));
		}
		
		counter = 0;
		for (Node node : neighbors){
			counter += 1;
			if (counter > 100)
				break;
			if (node.getType().getName().equals("Article")){
				HashMap<String, String> articleInfo = new HashMap<String, String>();
				articleInfo.put("title", node.getName());
				try {
					articleInfo.put("content", ds.getAssociatedText(node));
				} catch (Exception e2) {
					articleInfo.put("content", "No Content.");
				}
				
				articleList.add(articleInfo);
			}
		}
		
		returnMap.put("persons", perList);
		returnMap.put("locations", locList);
		returnMap.put("organizations", orgList);
		returnMap.put("articles", articleList);
		
		return returnMap;
	}

}