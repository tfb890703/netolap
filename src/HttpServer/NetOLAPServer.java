package HttpServer;

import java.net.InetSocketAddress;
import java.util.Collection;

import network.DataSet;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;


public class NetOLAPServer {
	
	DataSet ds;

    public static void main(String[] args) throws Exception {
    	
    	//Build the network
    	DataSet ds = DataSet.loadDataSet("data/nyt2013_ready.zip");
    	
        HttpServer server = HttpServer.create(new InetSocketAddress(8800), 0);
        
        HttpContext contextQuery;
        HttpContext entityLookup;
        
        contextQuery = server.createContext("/query", new QueryHandler(ds));
        entityLookup = server.createContext("/entity", new EntityHandler(ds));
        
        contextQuery.getFilters().add(new ParameterFilter());
        entityLookup.getFilters().add(new ParameterFilter());
        server.setExecutor(null); // creates a default executor
        server.start();
        System.out.println("Server started");
    }
    
}

