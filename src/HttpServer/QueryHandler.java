package HttpServer;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


import network.DataSet;
import network.Network;
import network.Node;
import network.NodeType;
import network.Subnetwork;
import network.Utils;



import Algorithm.NetworkMetricsAlgoResult;
import Algorithm.RankingAlgoResult;
import Algorithm.SentimentAnalysisAlgoResult;
import Algorithm.SimilaritySearchAlgoResult;
import OLAP.Parser;

import com.google.gson.GsonBuilder;
import com.sun.org.apache.xpath.internal.operations.And;


public class QueryHandler extends JSONHandler  {
	
	DataSet ds;
	
	public QueryHandler(DataSet ds){
		this.ds = ds;
		// We do not want to return all the node fields to client
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}
	
	/* (non-Javadoc)
	 * @see HttpServer.JSONHandler#processRequest(java.util.Map)
	 */
	@Override
	public Object processRequest(Map<String, Object> params) {
		System.out.println("Start querying");
		String statement = (String)params.get("query");
		
		//TODO send the result back to the frontend
		Object output = Parser.parseStatement(ds, statement);
		
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		//If this is CREATE NETWORK clause
		if (output.getClass().equals(Subnetwork.class)){
            returnMap.put("value", "Network " + ((Subnetwork)output).getName() + " is created.");

            //build json object
            HashMap<String, String> jsonObj = new HashMap<String, String>();
            jsonObj.put("createdNetworkName", ((Subnetwork) output).getName());
            returnMap.put("jsonResult", jsonObj);
        }
		//If this is similarity clause
		else if (output.getClass().equals(SimilaritySearchAlgoResult.class)){
			SimilaritySearchAlgoResult result = (SimilaritySearchAlgoResult)output;
			String tempStr = "";
            ArrayList<HashMap<String, String>> jsonResult = new ArrayList<HashMap<String, String>>();
            for (Node n : result.similarityScores.keySet()){
				LinkedHashMap<Node, Double> myMap = (LinkedHashMap<Node, Double>) Utils.sortByValues(result.similarityScores.get(n)); 
				for (Node neighbor : myMap.keySet()){
					tempStr += neighbor.getName() + ":	" + String.format("%.3f", myMap.get(neighbor)) + "<br>";

                    //build json object
                    HashMap<String, String> curNodeProperties = new HashMap<String, String>();
                    curNodeProperties.put("name", neighbor.getName());
                    curNodeProperties.put("score", "" + myMap.get(neighbor));
                    curNodeProperties.put("id", "" + neighbor.getId());
                    jsonResult.add(curNodeProperties);
                }
			}
			returnMap.put("value", tempStr);
            returnMap.put("jsonResult", jsonResult);
		}
		else if (output.getClass().equals(RankingAlgoResult.class)){
			RankingAlgoResult result = (RankingAlgoResult)output;
            HashMap<String, Object> jsonResult = new HashMap<String, Object>();

            StringBuilder tempStr = new StringBuilder();
			for (NodeType nt : result.rankingByType.keySet()){
				tempStr.append(nt.getName() + "<br>");

                //build json object
                ArrayList<Object> rankingForType = new ArrayList<Object>();
                jsonResult.put(nt.getName(), rankingForType);

                LinkedHashMap<Node, Double> myMap = (LinkedHashMap<Node, Double>) Utils.sortByValues(result.rankingByType.get(nt));
				int count = 0;
				for (Node node : myMap.keySet()){
					count += 1;
					if (count > 20){
						break;
					}
					tempStr.append(node.getName() + ":         " + String.format("%.3f", myMap.get(node)) + "<br>");

                    //Build json object
                    HashMap<String, String> curNodeProperties = new HashMap<String, String>();
                    curNodeProperties.put("name", node.getName());
                    curNodeProperties.put("id", ""+node.getId());
                    curNodeProperties.put("score", ""+myMap.get(node));
                    rankingForType.add(curNodeProperties);

                }
				tempStr.append("<br><br><br>");
			}
			returnMap.put("value", tempStr.toString());
            returnMap.put("jsonResult", jsonResult);
		}
		else if (output.getClass().equals(SentimentAnalysisAlgoResult.class)){
			SentimentAnalysisAlgoResult result = (SentimentAnalysisAlgoResult)output;
			StringBuilder tempStr = new StringBuilder();

            HashMap<String, Object> jsonResult = new HashMap<String, Object>();

            for (NodeType nt : result.sentiScoreByType.keySet()){
				tempStr.append(nt.getName() + "<br>");

                //build json object
                ArrayList<Object> sentimentForType = new ArrayList<Object>();
                jsonResult.put(nt.getName(), sentimentForType);

                LinkedHashMap<Node, Double> myMap = (LinkedHashMap<Node, Double>) Utils.sortByValues(result.sentiScoreByType.get(nt));
				int count = 0;
				for (Node node : myMap.keySet()){
					count += 1;
					if (count > 20 && count < myMap.size() - 20) {
						continue;
					}
					tempStr.append(node.getName() + ":         " + String.format("%.3f", myMap.get(node)) + "<br>");

                    //build json object
                    HashMap<String, String> curNodeProperties = new HashMap<String, String>();
                    curNodeProperties.put("name", node.getName());
                    curNodeProperties.put("id", ""+node.getId());
                    curNodeProperties.put("score", ""+myMap.get(node));
                    sentimentForType.add(curNodeProperties);

                }
				tempStr.append("<br><br><br>");
			}
			returnMap.put("value", tempStr.toString());
            returnMap.put("jsonResult", jsonResult);
		}
		else if (output.getClass().equals(NetworkMetricsAlgoResult.class)){
			NetworkMetricsAlgoResult result = (NetworkMetricsAlgoResult)output;
			String tempStr = "";
			tempStr += "Node Count = " + result.metrics.get("Node Count") + "<br><br>";
			tempStr += "Edge Count = " + result.metrics.get("Edge Count") + "<br><br>";
			tempStr += "Average Edge Count = " + result.metrics.get("Average Edge Count") + "<br><br>";

            HashMap<String, Object> jsonResult = new HashMap<String, Object>();
            jsonResult.put("NodeCount", "" + result.metrics.get("Node Count"));
            jsonResult.put("EdgeCount", "" + result.metrics.get("Edge Count"));
            jsonResult.put("AverageEdgeCount", "" + result.metrics.get("Average Edge Count"));


            tempStr += "<br>Typed Node Count<br>";
            jsonResult.put("TypedNodeCounts", new HashMap<String, Object>());
            for (String str : ((HashMap<String, Integer>)result.metrics.get("Typed Node Count")).keySet()){
                int value = ((HashMap<String, Integer>)result.metrics.get("Typed Node Count")).get(str);
				tempStr += str + ": " +  value + "<br>";
                ((HashMap<String, Object>) jsonResult.get("TypedNodeCounts")).put(str, ""+value);
            }
			
			tempStr += "Average Typed Edge Count<br>";
            jsonResult.put("AverageTypedEdgeCounts", new HashMap<String, Object>());
			for (String str : ((HashMap<String, Double>)result.metrics.get("Average Typed Edge Count")).keySet()){
                double value = ((HashMap<String, Double>)result.metrics.get("Average Typed Edge Count")).get(str);
				tempStr += str + ": " +  value + "<br>";
                ((HashMap<String, Object>) jsonResult.get("AverageTypedEdgeCounts")).put(str, ""+value);
            }
			
			returnMap.put("value", tempStr);
            returnMap.put("jsonResult", jsonResult);
		}
		
		returnMap.put("graph", String.valueOf(Parser.count) + ".json");
		
		return returnMap;
		
//		
//		String query = null;
//		if(params.containsKey("q"))
//			query = params.get("q").toString();
//		
//		if (query == null)
//			return null;
//		
//		query = query.replace('+', ' ');
//		
//		Object[] authors;
//		try {
//			SortedMap<String, Author> resultMap = trie.getPrefixedBy(query.toLowerCase());
//			if (resultMap.size() == 0)
//				return new ArrayList<>();
//			
//			authors = resultMap.values().toArray();
//			Arrays.sort(authors, new Comparator<Object>() {
//				@Override
//				public int compare(Object o1, Object o2) {
//					Author author1 = (Author)o1;
//					Author author2 = (Author)o2;
//					
//					if (author1.getPublications() == null)
//						return 1;
//					if (author2.getPublications() == null)
//						return -1;
//					
//					return author2.getPublications().size() - author1.getPublications().size();
//				}
//			});
//		}catch (Exception e) {
//			e.printStackTrace();
//			System.err.println("Error");
//			return null;
//		}
//		
//		return Arrays.copyOfRange(authors, 0, Math.min(10, authors.length));
	}

}