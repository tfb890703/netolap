/**
 * 
 */
package HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Responds to requests by JSON string
 * @author Xiao Cheng
 * 
 */
public abstract class JSONHandler implements HttpHandler {
	
	protected Gson gson;
	
	public JSONHandler(){
		gson = new Gson();
	}

	@SuppressWarnings("unchecked")
	public void handle(HttpExchange t) {
		Map<String, Object> params = (Map<String, Object>) t.getAttribute("parameters");
		
		InputStreamReader isr;
		try {
			isr = new InputStreamReader(t.getRequestBody(),"utf-8");
		    BufferedReader br = new BufferedReader(isr);
            String query = br.readLine();
            System.out.println(query);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
		
		byte[] responseBytes = serialize(processRequest(params));
		
		try {
			t.sendResponseHeaders(200, responseBytes.length);
			OutputStream os = t.getResponseBody();
			os.write(responseBytes);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error sending response of Content-Length:" + responseBytes.length);
		}
	}
	
	public byte[] serialize(Object o){
		String jsonString = gson.toJson(o);
		return jsonString.getBytes();
	}
	

	/**
	 * Implement the input/output paradigm to send JSON strings
	 * @param parameters
	 * @return
	 */
	public abstract Object processRequest(Map<String, Object> parameters);

}
