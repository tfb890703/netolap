# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import zipfile
import json

def parse_json(as_str): 
    obj_all = json.loads(as_str)
    articles = obj_all['response']['docs']
    return articles
    
def read_zip(filepath):
    all_articles = []
    with zipfile.ZipFile(filepath) as collection:
        #print collection.namelist()
        for fn in collection.namelist():
            if not fn.endswith('.json'):
                continue
            with collection.open(fn) as f:
                as_str = f.read()
                new_articles = parse_json(as_str)
                all_articles += new_articles
            #break
    print 'got ' + str(len(all_articles)) + ' articles'
    return all_articles



# <codecell>

def get_urls(articles): 
    urls = []
    for article in articles: 
        if 'web_url' in article: 
            urls.append( article['web_url'] )
    return urls

#all_articles = read_zip('json_files.zip')
#urls = get_urls(all_articles)

len(urls)

# <codecell>

all_articles = read_zip('json_files.zip')

# <codecell>

def get_urls(articles): 
    urls = []
    for article in articles: 
        if 'web_url' not in article:
            continue
        if article['type_of_material'] != 'News':
            continue
        urls.append( article['web_url'] )
    return urls

urls = get_urls(all_articles)

# <codecell>

print len(urls)
#with open('news_urls.txt', 'w') as f_urls:
#    for url in urls: 
#        f_urls.write(url + '\n')
print urls[9471]


# <codecell>

#Get some statistics

#interesting headings: ['type_of_material', 'source', 'section_name', 'subsection_name']

import collections
import operator
counts = collections.defaultdict(int)
for article in all_articles: 
    if 'type_of_material' not in article:
        continue
    else: 
        cur_type = article['type_of_material']
        counts[cur_type] += 1
        
print sorted(counts.iteritems(), key=operator.itemgetter(1), reverse=True)
    
    

# <codecell>

counts = collections.defaultdict(int)
for article in all_articles: 
    if article['type_of_material'] != 'News':
        continue
    else: 
        cur_type = article['source']
        counts[cur_type] += 1
count_s = sorted(counts.iteritems(), key=operator.itemgetter(1), reverse=True)
print count_s

for item in count_s: 
    print item[0] + ' (' + str(item[1]) + '),'

# <codecell>


