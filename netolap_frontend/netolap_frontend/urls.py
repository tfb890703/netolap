from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('main.views',
	url(r'^$', 'homepage'),
	url(r'^query/$', 'query'),
	url(r'^network/$', 'network'),
	url(r'^test/$', 'test'),
	url(r'^entity/(?P<eid>\w+)/$', 'entity'),
)
