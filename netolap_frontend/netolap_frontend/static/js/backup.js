$(document).ready(function() { 
	var min_dish_num = 0;
	var max_dish_num = 0;
	var basic_price = 0;
	var increment_price = 0;

	var a = 0;

	$("#stop_picker").change(function(){
		var val = $("#stop_picker").val();
		$.post("/changeStop/"+val+"/",function(data){
			window.location = window.location;
		});
	});
});

$(function() {
	var a = 0;
	// Charts data

	// Personal Info Page
	$('#profile li').on('click', function() {
		$('#profile li').removeClass('active');
		$(this).addClass('active');
		var tab = this.children[0].hash.split('#')[1];
		//currentTab.removeClass('active');
		$('.tab-pane').removeClass('active');
		$('#' + tab).addClass('active');
	});



	$('#rechargebutton').click(function(){
		data = {};
		data['promotecode'] = $('input.promotecode')[0].value;
		data['promotecode2'] = $('input.promotecode2')[0].value;

		// 
		$.ajax({
			type : "POST",
			data : data,
			url : "/recharge/"
		}).done(function(data) {
			alert(data['msg']);
		}); 
	});


	$('#loginform').submit(function () {
		 return false;
	});


	$('#signupmodal').submit(function () {
		 return false;
	});


	$('#categoryselector').change(function() {
		$('#deallist').attr('style', "display:none;");

  		category = $('#categoryselector').val();
		res = $('#categoryselector').attr('res');

		data = {}
		data['category'] = category;
		data['res'] = res;

		$.ajax({
			type : "POST",
			data : data,
			url : "/getitems/"
			}).done(function(data) {
				if (data['status'] == 'fail')
				{
					alert(data['msg']);
					return;
				}
				else
				{
					$('#dishlist').empty();
					for (i = 0; i < data['dishes'].length; i++)
					{
						dish = data['dishes'][i];
						str = '<div class="span3 itemwide" style="height:185px;"><div style="height: 40px;" ><h6><span title="';
						str += dish['name'];
						str += '">';
						str += dish['name'];
						str += '</span></h6></div><img title="';
						str += dish['name'];
						str += '" src="';
						str += dish['image'];
						str += '" class="img-polaroid imgsize" onClick="showLargeImage(this)"><div class="row-fluid" style="margin-top:5px;"><div class="span4">$';
						str += parseFloat(dish['price']);
						str += '</div><div class="span3"><select class="selectbar dishnum"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select></div><div class="span3 offset2"><button class="btn btn-mini btn-success" itemtype="Normal" taxrate ="'
						str += parseFloat(dish['taxrate']);
						str += '" itemprice="';
						str += parseFloat(dish['price']);
						str += '" itemid="';
						str += parseInt(dish['id']);
						str += '" type="button" itemname="';
						str += dish['name'];
						str += '" onclick="addDish(this)">ADD</button></div></div></div>';

						
						$('#dishlist').append(str);
					}
					return;
				}
			}); 
		});


		$('#managecategoryselector').change(function() {
	  		category = $('#managecategoryselector').val();
			res = $('#managecategoryselector').attr('res');

			data = {}
			data['category'] = category;
			data['res'] = res;

			post_to_url("/manage/getitems/", data);
		});

	// $('#votemodal').modal({show: true});
});

function loginWithFacebook() {
    FB.api('/me', function(response) {
        data = {};
        data['email'] = response.email;
        data['name'] = response.name;

        console.log(response);
    });
}


function dealdetail(cb){
	did = $(cb).attr('itemid');	
	dname = $(cb).attr('itemname');

	res = $('#categoryselector').attr('res');


	data = {}
	data['did'] = did;
	data['res'] = res;

	$.ajax({
		type : "POST",
		data : data,
		url : "/getdealdetail/"
		}).done(function(data) {
			if (data['status'] == 'fail')
			{
				alert(data['msg']);
				return;
			}
			else
			{
				$('#dealdetailmodal').modal({show: true});
				$('#dealdishlist').empty();
				for (i = 0; i < data['dishes'].length; i++)
				{
					dish = data['dishes'][i];
					str = '<div class="span3 itemwide" style="height:185px;"><div style="height: 40px;" ><h6><span title="';
					str += dish['name'];
					str += '">';
					str += dish['name'];
					str += '</span></h6></div><img title="';
					str += dish['name'];
					str += '" src="';
					str += dish['image'];
					str += '" class="img-polaroid imgsize" onClick="showLargeImage(this)"><div class="row-fluid" style="margin-top:5px;"><div class="span4">$';
					str += parseFloat(dish['price']);
					str += '</div><div class="span4 offset4"><strong>X '
					str += dish['count'];
					str += '</strong></div></div></div>';
					$('#dealdishlist').append(str);
				}
				$('#dealdetailmodal #myModalLabel').html(dname);
				return;
			}
		}); 
}

function voteNotify(){
	if ($('#neverask').is(':checked'))
	{
		$.ajax({
			type : "POST",
			data : {},
			url : "/voteNotify/"
		}).done(function(data) {

			$('#votemodal').modal('hide');
		}); 
	}
	else
	{
		$('#votemodal').modal('hide');
	}
}

function voteClick()
{
	if ($('#neverask').is(':checked'))
	{
		$.ajax({
			type : "POST",
			data : {},
			url : "/voteNotify/"
		}).done(function(data) {
			window.location = "https://docs.google.com/spreadsheet/viewform?formkey=dGdqVmR4MC1ZelR3bHE2TGVmUk50Y2c6MA";
			$('#votemodal').modal('hide');
		}); 
	}
	else
	{
		window.location = "https://docs.google.com/spreadsheet/viewform?formkey=dGdqVmR4MC1ZelR3bHE2TGVmUk50Y2c6MA";
		$('#votemodal').modal('hide');
	}
}

function showdeals(){
	$('#deallist').attr('style', '');
}

function updateitem(cb){
	did = $(cb).attr('dishid');
	$.ajax({
		type : "POST",
		data: $('#' + did).serialize(),
		url : "/manage/update/"
	}).done(function(data) {
		alert(data['msg']);
	}); 
}

function deleteitem(cb){
	did = $(cb).attr('dishid');
	$.ajax({
		type : "POST",
		data: $('#' + did).serialize(),
		url : "/manage/delete/"
	}).done(function(data) {
		if (data['status'] == 'fail'){
			alert(data['msg']);	
		}
		else{
			alert(data['msg']);
			category = $('#managecategoryselector').val();
			res = $('#managecategoryselector').attr('res');

			data = {}
			data['category'] = category;
			data['res'] = res;

			post_to_url("/manage/getitems/", data);

		}
	}); 
}

function couponsubmit(){
	$.ajax({
		type : "POST",
		data: $('#couponform').serialize(),
		url : "/coupon/"
	}).done(function(data) {
		if (data['status'] == 'fail'){
			alert(data['msg']);	
		}
		else{
			alert(data['msg']);
		}
	}); 
}

function createitem(cb){
	$.ajax({
		type : "POST",
		data: $('#createitemform').serialize(),
		url : "/manage/create/"
	}).done(function(data) {
		if (data['status'] == 'fail'){
			alert(data['msg']);	
		}
		else{
			alert(data['msg']);
			category = $('#managecategoryselector').val();
			res = $('#managecategoryselector').attr('res');

			data = {}
			data['category'] = category;
			data['res'] = res;

			post_to_url("/manage/getitems/", data);

		}
	}); 
}

function showdeleteitemconfirm(cb){
	$('#deleteitemconfirm').attr('dishid', $(cb).attr('dishid'));
	$('#deleteitemmodal').modal('show');
}

function getTime(time){
	var h = parseInt((time / 100)).toString();
	var m = (time % 100).toString();
	if (m.length == 1)
		m = '0' + m;
	return h + ":" + m;
}

// Add dishes into the order
function addDish(cb){
	var $table = $("#cartlist");
	$table.show();

	// 0=id, 1=type, 2=name, 3=prices, 4=amount
	dishes = getDishes();
	  
	amount = parseInt($(cb).parent().siblings().children(".dishnum").val());
	itemtype = $(cb).attr('itemtype');
	itemid = $(cb).attr('itemid');
	itemname = $(cb).attr('itemname');
	itemprice = $(cb).attr('itemprice');
	taxrate = $(cb).attr('taxrate');
	have = false;

	jQuery.each(dishes, function(i, dish) {
	if (itemid == dish[0])
	{
		have = true;
		dish[4] += amount;
		return false;
	}
	});
	if (have == false)
	{
		dishes[dishes.length] = [];
		dishes[dishes.length - 1][0] = itemid;
		dishes[dishes.length - 1][1] = itemtype;
		dishes[dishes.length - 1][2] = itemname;
		dishes[dishes.length - 1][3] = itemprice;
		dishes[dishes.length - 1][4] = amount;
		dishes[dishes.length - 1][5] = taxrate;
	}
	
	if (calculatePrice(dishes))
		listDishes(dishes);
}

// Add dishes into the order
function deleteDish(cb){
	// 0=id, 1=type, 2=name, 3=prices, 4=amount
	dishes = getDishes();

	//get itemid
	itemid = $(cb).attr('itemid');
	idx = -1;
	
	jQuery.each(dishes, function(i, dish) {
	if (itemid == dish[0])
	{
		idx = i;
		return false;
	}
	});
	if (idx != -1)
	{
		dishes.splice(idx, 1);
	}

	listDishes(dishes);
	calculatePrice(dishes);
}

function showstop(cb){
	$('#stoppicmodal img').attr('src' , $(cb).attr('pic'));
	$('#stoppicmodal').modal({show: true});
}

function listDishes(dishes){
	var $table = $("#cartlist");
	var $tbody = $("#cartlist tbody");

	if (dishes.length == 0)
		$table.hide();

	// clear 
	$tbody.empty();
	jQuery.each(dishes, function(i, dish) {
	$tbody.append(
		$('<tr>').append($('<td>').attr('itemid', dish[0]).attr('itemtype', dish[1]).attr('taxrate', dish[5]).text(dish[2]))
				 .append($('<td>').text(dish[3]))
				 .append($('<td>').text(dish[4]))
				 .append($('<td>').append($('<a>').attr('href', '#').attr('itemid', dish[0]).attr('onclick', 'deleteDish(this)').text('X')))
				 .addClass("success")
	);
	});
}

function getDishes(){
	var $table = $("#cartlist"),
		$rows = $table.find("tbody tr"),
		$tbody = $("#cartlist tbody");

	dishes = [];
	  
	$rows.each(function(row,v) {
		dishes[row] = [];
		$(this).find("td").each(function(cell,v) {

			if (cell == 3)
				return true;
			
			if (cell == 0)
			{
				dishes[row][0] = $(this).attr('itemid');
				dishes[row][1] = $(this).attr('itemtype');
				dishes[row][5] = parseFloat($(this).attr('taxrate'));
			}
			dishes[row][cell + 2] = $(this).text();
		});
		dishes[row][4] = parseInt(dishes[row][4]);
	});

	return dishes;
}

function calculatePrice(dishes){

	// 0=id, 1=type, 2=name, 3=prices, 4=amount
	var totalPrice = 0;
	var tax = 0;
	

	$("#errorTag").html("");


	for (i = 0; i < dishes.length; i++)
	{
		dish = dishes[i];
		price = parseFloat(dish[3]);
		amount = dish[4];
		taxrate = dish[5];
		totalPrice += price * amount;
		tax += price * taxrate * amount / 100;
	}


	if (totalPrice > 300)
	{
		alert("We only accept single order for less than $300.00. Sorry for any inconvinience.");
		return false;
	}

	$("#priceTag").html(totalPrice.toFixed(2));
	$("#taxTag").html(tax.toFixed(2));

	if (totalPrice > 0)
	{
		deonryFee = calculateDeonryFee(totalPrice).toFixed(2);
		$("#deonryTag").html(deonryFee);
		$("#totalTag").html((parseFloat($("#deonryTag").html()) + parseFloat($("#priceTag").html()) + parseFloat($("#taxTag").html())).toFixed(2));
	}
	else
	{
		$("#deonryTag").html("0.0");
		$("#totalTag").html((parseFloat($("#deonryTag").html()) + parseFloat($("#priceTag").html()) + parseFloat($("#taxTag").html())).toFixed(2));
	}
	return true;
}

function calculateDeonryFee(price)
{
	//TODO delete
	return 0;
	if (price < 5)
		return 0.5;
	if (price < 20)
		return price * 0.1;
	if (price < 50)
		return 2 + (price - 20) * 0.09;
	if (price < 100)
		return 4.7 + (price - 50) * 0.06;

	return 7.7 + (price - 100) * 0.03;
}

function sendForm() {
 
  var
    oData = new FormData(document.forms.namedItem("fileinfo"));
  
  var oReq = new XMLHttpRequest();
  oReq.open("POST", "/manage/uploadDoc/", true);
  oReq.onload = function(oEvent) {
    if (oReq.status == 200) {
    	$('#createitemform img').attr('src', '/static/image/dish/' + $('#managecategoryselector').attr('res') + '_temp.jpg');
    	$('#createitemform .image').attr('value', '/static/image/dish/' + $('#managecategoryselector').attr('res') + '_temp.jpg');
		// element = document.getElementById("uploadButton");
		// element.parentNode.removeChild(element);
    } else {
      oOutput.innerHTML = "Error " + oReq.status + " occurred uploading your file.<br \/>"
    }
  };
 
  oReq.send(oData);
}

function showLargeImage(cb)
{
	$('#itemlargeimgmodal img').attr('src' , $(cb).attr('src'));
	$('#itemlargeimgmodal #myModalLabel').html($(cb).attr('itemname'));
	$('#itemlargeimgmodal').modal({show: true});
}

function showconfirmplace()
{
	total_price = $('tr.total_price').attr('total_price');
	buttonselected = $('#timetable button.selected')
	if (buttonselected.length == 0){
		alert('Please select the time!');
	}
	date = $(buttonselected[0]).attr('date');
	hour = $(buttonselected[0]).attr('time');

	info = "Your order is ready! The total price is $" + total_price.toString() + '.\r\n';
	info += "It will be delivered from the store on " + date.toString() + ", " + hour.toString() + ":00.";

	$('#confirmplacemodal .info').html(info);

	$('#confirmplacemodal').modal({show: true});
}

function show_paypal_by_order(userid, orderid, total_price){
	$('#paybyordermodal .amount').attr('value', total_price);
	return_url = "http://kantwait.com/paymentbyorder/" + orderid + "/" + userid + "/";
	$('#paybyordermodal .return').attr('value', return_url);
	$('#paybyordermodal .notify_url').attr('value', return_url);
	
	$('#paybyordermodal').modal({show: true});
}

function post_to_url(path, params, method) {
        method = method || "post";

        var form = document.createElement("form");

        //Move the submit function to another variable
        //so that it doesn't get overwritten.
        form._submit_function_ = form.submit;

        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }

        document.body.appendChild(form);
        form._submit_function_(); //Call the renamed function.
}

function redeem()
{
	var code = $('#inputCode').val();
	var codeagain = $('#inputCodeAgain').val();

	if (code != codeagain){
		document.getElementById("redeemerror").innerHTML = "Two codes dismatch";
		return;
	}

	if (code.length != 10 || codeagain.length != 10){
		document.getElementById("redeemerror").innerHTML = "Code length incorrect, please input 10 letters";
		return;
	}


	data = {};
	data['promotecode'] = code;
	data['promotecode2'] = codeagain;

	// 
	$.ajax({
		type : "POST",
		data : data,
		url : "/recharge/"
	}).done(function(data) {
		if (data['status'] == 'fail')
		{
			document.getElementById("redeemerror").innerHTML = data['msg'];
			return;
		}
		else
		{
			alert('Redeem Success!');
			window.location = "/";
			//Success for signup
			$('#redeemmodal').modal('hide');
		}
	}); 
}

function forgetemail()
{
	var email = $('#forgetEmail').val();
	if (email.length == 0){
		document.getElementById("forgeterror").innerHTML = "Please type in email";
		return;
	}

	if (!validateEmail(email)){
		document.getElementById("forgeterror").innerHTML = "Email Format Incorrect";
		return;
	}


	data = {};
	data['email'] = email;
	
	$.ajax({
			type : "POST",
			data : data,
			url : "/forgetsend/"
		}).done(function(data) {
			if (data['status'] == 'fail')
			{
				document.getElementById("forgeterror").innerHTML = data['msg'];
				return;
			}
			else
			{
				document.getElementById("forgetsendbutton").innerHTML="Resend";
				document.getElementById("forgeterror").innerHTML = data['msg'];
				return;
			}
		});
}

function resetpassword()
{
	var password = $('#resetPassword').val();
	var passwordagain = $('#resetPasswordAgain').val();
	var key = $("#confirm-key-forget").attr("key");
	var userid = $("#userid-forget").attr("key");
	

	if (passwordagain.length == 0 || password.length == 0){
		document.getElementById("signuperror").innerHTML = "Please type in password";
		return;
	}


	data = {};
	data['password'] = password;
	data['key'] = key;
	data['userid'] = userid;
	
	$.ajax({
			type : "POST",
			data : data,
			url : "/resetpasswordfinal/"
		}).done(function(data) {
			if (data['status'] == 'fail')
			{
				document.getElementById("reseterror").innerHTML = data['msg'];
				return;
			}
			else
			{
				document.getElementById("reseterror").innerHTML = data['msg'];
				return;
			}
		});
}

function checkoutlogin()
{
	console.log("Beginning.");
	var email = $('#checkoutloginEmail').val();
	var password = $('#checkoutloginPassword').val();	

	if (email.length == 0){
		alert('Email format not correct.')
		return;
	}

	if (!validateEmail(email)){
		alert('Email format not correct.')
		return;
	}


	if (password.length == 0){
		alert('Password is empty.')
		return;
	}

	data = {};
	data['email'] = email;
	data['password'] = password;

	$.ajax({
			type : "POST",
			data : data,
			url : "/login/"
		}).done(function(data) {
			if (data['status'] == 'fail')
			{
				alert(data['msg']);
				return;
			}
			else
			{
				window.location = "/checkout/";
				location.reload();
			}
		}); 
}

function login()
{
	var email = $('#loginEmail').val();
	var password = $('#loginPassword').val();
	var ifRemember = $('#loginRemember').val();

	if (email.length == 0){
		document.getElementById("loginerror").innerHTML = "Please type in email";
		return;
	}


	if (password.length == 0){
		document.getElementById("loginerror").innerHTML = "Please type in password";
		return;
	}

	// ajax
	data = {};
	data['email'] = email;
	data['password'] = password;
	data['ifRemember'] = ifRemember;
	data['type'] = 'normal';
	
	
	$.ajax({
			type : "POST",
			data : data,
			url : "/login/"
		}).done(function(data) {
			if (data['errorKey'] == 1) {
				$("#resendemail").show();
				$("#resendemail").click(function(e){
					e.preventDefault();
					var d = $("#loginEmail").val();
					$.post("/resendConfirmationEmail/",{
						email: d
					}).done(function(data) {
					  alert("Your confirmation email has been resent.");
					  $("#loginmodal").modal("hide");
					});
				});
			}
			if (data['status'] == 'fail')
			{
				document.getElementById("loginerror").innerHTML = data['msg'];
				return;
			}
			else
			{
				//alert('Sign in success!');
				window.location = "/";
				//Success for signin
				$('#loginmodal').modal('hide');
			}
		}); 
}

function signup()
{
	//first check severalthings
	var name = $('#inputName').val();
	var email = $('#inputEmail').val();
	var password = $('#inputPassword').val();
	var passwordagain = $('#inputPassword').val();
	var mobile = $('#inputMobile').val();

	if (name.length == 0){
		document.getElementById("signuperror").innerHTML = "Please type in name";
		return;
	}

	if (email.length == 0){
		document.getElementById("signuperror").innerHTML = "Please type in email";
		return;
	}

	// if (mobile.length == 0){
	// 	document.getElementById("signuperror").innerHTML = "Please type in mobile phone number";
	// 	return;
	// }

	if (!validateEmail(email)){
		document.getElementById("signuperror").innerHTML = "Email Format Incorrect";
		return;
	}

	if (password != passwordagain){
		document.getElementById("signuperror").innerHTML = "Confirm your password";
		return;
	}

	if (password.length < 8){
		document.getElementById("signuperror").innerHTML = "Password is too short (>8).";
		return;
	}


	document.getElementById("signuperror").innerHTML = "Signing up... Please wait.";


	// if (mobile.length > 15){
	// 	document.getElementById("signuperror").innerHTML = "Mobile phone number incorrect";
	// 	return;
	// }

	// ajax
	data = {};
	data['name'] = name;
	data['password'] = password;
	data['email'] = email;
	data['mobile'] = mobile;
	data['recaptcha_challenge_field'] = $('#recaptcha_challenge_field').val();
	data['recaptcha_response_field'] = $('#recaptcha_response_field').val();
	

	$.ajax({
			type : "POST",
			
			data : data,
			url : "/signup/"
		}).done(function(data) {
			if (data['status'] == 'fail')
			{
				document.getElementById("signuperror").innerHTML = data['msg'];
				return;
			}
			else
			{
				alert('Sign up successful! Please Check your email and activate your account.');
				window.location = "/";
				//Success for signup
				$('#signupmodal').modal('hide');
			}
		});
}

function validateEmail(email) { 
	var re = /\S+@\S+\.\S+/;
	return re.test(email);
}
