$(document).ready(function() { 


	$('#submit').click(function(){
		query = $('#query')[0].value;
		data = {};
		data['query'] = query;
		$.ajax({
			type : "POST",
			data : data,
			url : "http://127.0.0.1:8800/query/"
			// url : "/query/"
		}).done(function(data) {
			obj = jQuery.parseJSON( data );
			var fileName = '/static/graphs/' + obj['graph'];
			act(fileName);
			$('#history').append("<p>" + query + "</p>");
			$('#result').empty();
			
			$('#result').append("<p>" + obj['value'] + "</p>");
		});
	});
});