from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.template.context import RequestContext
from django.http import HttpResponse
from django.utils import simplejson
import json
import urllib2

def homepage(request):
    ''' Home page
    '''
    return render_to_response("site/navigator.html", {},  context_instance=RequestContext(request))

def network(request):
	''' OLAP page
	'''
	return render_to_response("site/index.html", {},  context_instance=RequestContext(request))

def query(request):
	query = request.POST['query']
	print query
	# call the java server to 
	return HttpResponse(simplejson.dumps({'status':'success', 'msg':'Information updated.'}),mimetype="application/json")


def test(request):
	return render_to_response("site/test.html", {},  context_instance=RequestContext(request))


def entity(request, eid):
	j = urllib2.urlopen('http://127.0.0.1:8800/entity?id='+eid)
	author_json = json.load(j)
	print author_json['name']
	print author_json['id']
	print author_json['url']
	print author_json['image']
	print len(author_json['persons'])
	# print author_json
	return render_to_response("site/entity.html", author_json,  context_instance=RequestContext(request))